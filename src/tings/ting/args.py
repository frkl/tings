# -*- coding: utf-8 -*-
from typing import Any, Dict

from frkl.common.dicts import get_seeded_dict
from tings.ting import SimpleTing, TingMeta


class ArgTing(SimpleTing):
    def __init__(self, name: str, meta: TingMeta, defaults: Dict[str, Any] = None):

        super().__init__(name=name, meta=meta)
        if defaults is None:
            defaults = {"arg_type": "string", "arg_required": True}
        self._defaults = defaults

    async def retrieve(self, *value_names: str, **requirements) -> Dict[str, Any]:

        cached_dict = requirements.get("_cached_dict", None)
        if cached_dict is None:
            cached_dict = get_seeded_dict(self._defaults, requirements["arg_dict"])

        result = {"_cached_dict": cached_dict}

        if "arg_type" in value_names:
            result["arg_type"] = cached_dict["arg_type"]

        if "arg_required" in value_names:
            result["arg_required"] = cached_dict["arg_required"]

        if "arg_default" in value_names:
            result["arg_default"] = cached_dict.get("default", None)

        if "arg_doc" in value_names:
            result["arg_doc"] = cached_dict.get("arg_doc", {})

        return result

    def provides(self) -> Dict[str, str]:

        return {
            "arg_type": "string",
            "arg_required": "bool",
            "arg_doc": "dict",
            "arg_default": "any",
        }

    def requires(self) -> Dict[str, str]:

        return {"arg_dict": "dict"}
