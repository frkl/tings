# -*- coding: utf-8 -*-
import os
from pathlib import Path
from typing import Any, AsyncIterator, Dict, Hashable, List, MutableMapping, Union

from anyio import open_file
from frkl.args.arg import RecordArg
from frkl.common.defaults import DEFAULT_EXCLUDE_DIRS
from frkl.common.exceptions import FrklException
from frkl.common.files.finding import FileMatcher, PathRegexMatcher, file_matches
from frkl.common.strings import generate_valid_identifier
from tings.defaults import TINGS_ITEM_ABSENT_KEY
from tings.makers import TingMaker
from tings.ting import Ting, TingMeta


class TextFileTingMaker(TingMaker):
    def __init__(
        self,
        name: str,
        meta: TingMeta,
        prototing: str,
        ting_name_strategy: str = None,
        ting_target_namespace: str = None,
        base_paths: Union[Path, str, List[Union[Path, str]]] = None,
        file_content: str = None,
        file_matchers: Union[List, Dict] = None,
        search_exclude_dirs=DEFAULT_EXCLUDE_DIRS,
    ):

        self._base_paths: List[str] = []
        if base_paths is None:
            base_paths = []
        elif isinstance(base_paths, (str, Path)):
            base_paths = [base_paths]

        self.add_base_paths(*base_paths)

        self._file_content = file_content

        if file_matchers is None:
            file_matchers = []

        self._matchers: List[FileMatcher] = []

        for m in file_matchers:
            if m.get("type", None) == "extension":
                regex = m["regex"]
                self._matchers.append(PathRegexMatcher(regex=regex))
            else:
                raise FrklException(
                    msg="Can't create TextFileTingMaker object.",
                    reason=f"Invalid or missing type in matcher configuration: {m}",
                )
        self._search_exclude_dirs = search_exclude_dirs

        super().__init__(
            name=name,
            prototing=prototing,
            ting_name_strategy=ting_name_strategy,
            ting_target_namespace=ting_target_namespace,
            meta=meta,
        )

    def add_base_paths(self, *base_paths: Union[str, Path]):

        temp = []
        for bp in base_paths:
            if isinstance(bp, str):
                temp.append(os.path.realpath(os.path.expanduser(bp)))
            elif isinstance(bp, Path):
                temp.append(Path(bp).resolve().as_posix())
            else:
                raise TypeError(
                    f"Can't add base path, invalid base path type '{type(bp)}': {bp}"
                )

        for bp in temp:
            if bp not in self._base_paths:
                self._base_paths.append(bp)

    def create_equality_obj(self, id: Hashable) -> Any:

        _id = self._check_id(id)
        if os.path.exists(_id):
            return os.path.getmtime(_id)
        else:
            return None

    def _is_in_base_paths(self, path: str):

        if not self._base_paths:
            return True

        path = os.path.realpath(os.path.expanduser(path))
        for bp in self._base_paths:
            if path.startswith(bp):
                return True
        else:
            return False

    def _check_id(self, id: Hashable) -> str:
        if not isinstance(id, str):
            raise FrklException(
                msg=f"'FileMaker' can't retrieve item '{id}'.",
                reason=f"Invalid type '{type(id)}', string required",
            )

        return id

    async def get_item(
        self, id: Hashable, required_values: RecordArg
    ) -> Union[str, MutableMapping[str, Any], None]:

        _id = self._check_id(id)
        if not self._is_in_base_paths(_id):
            return TINGS_ITEM_ABSENT_KEY

        if not os.path.exists(_id):
            return TINGS_ITEM_ABSENT_KEY

        # result = wrap_async_task(self._read_file, _id, required_values)
        result = await self._read_file(_id, required_values)

        return result

    async def _read_file(self, path: str, required_values: RecordArg):

        async with await open_file(path) as f:
            text_content = await f.read()

        result = self.process_raw_ting_content(
            text_content, required_values=required_values
        )

        if "ting_make_metadata" in required_values.arg_names:
            ting_make_metadata: Dict[str, Any] = {}
            ting_make_metadata["last_modified_timestamp"] = os.path.getmtime(path)
            ting_make_metadata["file_size"] = os.path.getsize(path)
            ting_make_metadata["full_path"] = os.path.realpath(path)
            for base_path in self._base_paths:
                if base_path in path:
                    ting_make_metadata["rel_path"] = os.path.relpath(path, base_path)
                    ting_make_metadata["base_path"] = base_path
                    break

            result["ting_make_metadata"] = ting_make_metadata

        return result

    def generate_ting_name(self, id: Hashable):

        ting_name = generate_valid_identifier(name=id)
        return ting_name

    async def add_file(self, path: Union[str, Path]) -> Ting:

        if isinstance(path, Path):
            path = path.resolve().as_posix()
        else:
            path = os.path.realpath(path)

        if not self._is_in_base_paths(path):
            raise FrklException(
                msg=f"Can't add file '{path}'.",
                reason=f"File is not child of any of the valid base paths: {', '.join(self._base_paths)}",
            )

        result = await self.make_ting(id=path)
        return result

    # async def update_file(self, path):
    #
    #     return await self.update_ting(path)

    async def find_all_ids(self) -> AsyncIterator[Hashable]:

        if not self._base_paths:
            for path in self._ids.keys():
                yield path

            return

        for base_path in self._base_paths:
            for root, dirnames, filenames in os.walk(base_path, topdown=True):

                if self._search_exclude_dirs:
                    dirnames[:] = [
                        d for d in dirnames if d not in self._search_exclude_dirs
                    ]

                for filename in [
                    f
                    for f in filenames
                    if file_matches(
                        path=os.path.join(root, f), matcher_list=self._matchers
                    )
                ]:

                    path = os.path.join(root, filename)

                    yield path
