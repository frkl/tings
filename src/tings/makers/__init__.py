# -*- coding: utf-8 -*-
import collections
import logging
import os
from abc import abstractmethod
from enum import Enum
from typing import (
    TYPE_CHECKING,
    Any,
    AsyncIterator,
    Dict,
    Hashable,
    Mapping,
    MutableMapping,
    Optional,
    Union,
)

import arrow
from frkl.args.arg import RecordArg
from frkl.common.exceptions import FrklException
from frkl.common.formats.auto import auto_parse_string
from frkl.common.strings import generate_valid_identifier
from tings.defaults import TINGS_ITEM_ABSENT_KEY
from tings.exceptions import TingException
from tings.ting import SimpleTing, Ting, TingMeta
from tings.ting.proto import ProtoTing


if TYPE_CHECKING:
    from tings.tingistry import Tingistry

log = logging.getLogger("tings")


class TingSyncEvent(Enum):

    ADDED = 1
    UPDATED = 2
    REMOVED = 3


class TingMaker(SimpleTing):
    def __init__(
        self,
        name: str,
        meta: TingMeta,
        prototing: str,
        ting_name_strategy: str = None,
        ting_target_namespace: str = None,
    ):

        self._ting_name_strategy = ting_name_strategy
        self._ting_target_namespace = ting_target_namespace

        self._prototing_name: str = prototing

        if meta is None:
            raise Exception(
                f"Can't create TingMaker '{name}'. No metadata provided, so can't access tingistry."
            )

        self._tingistry_obj: "Tingistry" = meta.tingistry
        self._prototing: ProtoTing = self._tingistry_obj.get_ting(  # type: ignore
            self._prototing_name
        )

        if not self._prototing:
            raise FrklException(
                msg=f"Can't create ting maker '{name}'.",
                reason=f"No prototing '{prototing}' available.",
            )

        self._input_spec: RecordArg = self._prototing.ting_input_args

        # self._arg = self._tingistry_obj.arg_hive.create_record_arg(
        #     childs=self._input_spec, id=f"{prototing}_input"
        # )

        self._synced = None

        self._tings: Dict[str, Ting] = {}
        self._ids: Dict[Hashable, str] = {}
        self._equality: Dict[Hashable, Any] = {}

        super().__init__(name=name, meta=meta)

    def provides(self) -> Mapping[str, Union[str, Mapping[str, Any]]]:

        return {}

    async def retrieve(self, *value_names: str, **requirements) -> Mapping[str, Any]:

        return {}

    def required_values(self) -> RecordArg:

        return self._input_spec

    def generate_ting_name(self, id: Hashable):

        ting_name = generate_valid_identifier(name=f"{self.__class__.__name__}_{id}")
        return ting_name

    def process_raw_ting_content(
        self, raw_content: str, required_values: RecordArg
    ) -> MutableMapping[str, Any]:

        result: Dict[str, Any] = {"ting_string": raw_content}
        if "ting_dict" in required_values.arg_names:
            dict_obj = auto_parse_string(raw_content)
            result["ting_dict"] = dict_obj

        return result

    async def make_ting(self, id: Hashable, ting_name: str = None) -> Ting:

        try:
            if ting_name is None:

                if self._ting_name_strategy is None:
                    ting_name = self.generate_ting_name(id)
                elif self._ting_name_strategy == "basename_no_ext":
                    path = str(id)
                    temp = os.path.basename(path)
                    if "." in temp:
                        temp = temp.split(".")[0]
                    ting_name = temp
                else:
                    raise Exception(
                        f"Invalid ting name strategy: {self._ting_name_strategy}"
                    )

            item = await self.get_item(id, self.required_values())

            if item is None:
                raise FrklException(
                    msg=f"Can't create ting from '{id}'.",
                    reason="No ting item created by maker.",
                )
            elif not isinstance(item, collections.abc.MutableMapping):
                raise FrklException(
                    msg=f"Can't create ting from '{id}'.",
                    reason=f"Invalid return type '{type(item)}' for item: {item}",
                )

            self._augment_item(item)

            if "ting_string" in item.keys():
                try:
                    parsed = auto_parse_string(item["ting_string"])
                    if isinstance(parsed, collections.abc.Mapping):
                        for k, v in parsed.items():
                            if k not in item.keys():
                                item[k] = v
                except Exception:
                    log.debug(f"Can't parse content of item {id}.", exc_info=True)

            missing = []
            for _arg_name, _arg in self.required_values().childs.items():

                if _arg_name not in item.keys():
                    if _arg.required and _arg.default is None:
                        missing.append(_arg_name)
                    else:
                        item[_arg_name] = {}

            if missing:
                raise FrklException(
                    msg=f"Can't add item: {ting_name}",
                    reason=f"Missing key(s) in dict content: {', '.join(missing)}",
                )

            if id is None:
                id = ting_name

            if id in self._ids.keys():
                raise TingException(
                    msg=f"Can't add ting '{ting_name}'to maker index.",
                    reason="Id already registered.",
                )

            if self._ting_target_namespace:
                ting_name = ".".join([self._ting_target_namespace, ting_name])
            ting = self._tingistry_obj.create_ting(
                prototing=self._prototing_name, ting_name=ting_name
            )

            self._ids[id] = ting_name
            self._tings[ting_name] = ting

            ting.set_input(_ignore_errors=True, **item)

            equality_obj = self.create_equality_obj(id)

            if equality_obj is None:
                self._equality[id] = item
            else:
                self._equality[id] = equality_obj

            return ting
        except Exception as e:
            log.debug(f"Can't make ting '{id}': {e}", exc_info=True)
            log.error(f"Can't make ting '{id}': {e}")
            raise e

    def get_ting(self, id: Hashable) -> Optional[Ting]:

        ting_name = self._ids.get(id, None)
        if ting_name is None:
            return None

        return self._tings.get(ting_name, None)

    def create_equality_obj(self, id: Hashable):

        return None

    @abstractmethod
    async def get_item(
        self, id: Hashable, required_values: RecordArg
    ) -> Union[str, MutableMapping[str, Any], None]:
        """Gets called when updating.

        Return tings.defaults.TINGS_ITEM_ABSENT_KEY if the item is gone.
        Return None and nothing will be done. Return a Mapping, and the ting will be updated.
        """

        pass

    def _augment_item(self, item: MutableMapping[str, Any]):

        if "ting_make_metadata" in self.required_values().arg_names:
            item.setdefault("ting_make_metadata", {})["maker"] = self.full_name
            item["ting_make_metadata"]["tingistry"] = self._tingistry_obj.full_name
        if "ting_make_timestamp" in self.required_values().arg_names:
            item["ting_make_timestamp"] = str(arrow.now())

    async def update_ting(self, id: Hashable) -> Optional[Ting]:

        ting = self.get_ting(id)

        if ting is None:
            raise FrklException(
                msg=f"Can't update ting '{id}'", reason="No ting with that id exists."
            )

        new_equality_obj = self.create_equality_obj(id)
        if new_equality_obj is not None:
            if self._equality[id] == new_equality_obj:
                log.debug(f"Not updating item {id}, equality items don't differ.")
                return ting

        new_item = await self.get_item(id, self.required_values())
        if new_item is None:
            return ting
        elif new_item == TINGS_ITEM_ABSENT_KEY:
            await self.remove_ting(ting)
            return None
        elif not isinstance(new_item, collections.abc.MutableMapping):
            raise FrklException(
                f"Can't update ting '{id}'.",
                reason=f"Invalid return type '{type(new_item)}' from 'get_item' child method: {new_item}",
            )

        self._augment_item(new_item)

        ting.set_input(**new_item)
        return ting

    async def remove_ting(self, ting: Union[str, Ting]) -> Ting:

        if isinstance(ting, str):
            ting_key: Optional[Hashable] = ting
            ting_name: Optional[str] = self._ids.pop(ting, None)
            if ting_name is None:
                if ting_key not in self._tings.keys():
                    raise TingException(
                        msg=f"Can't remove Ting '{ting_key}'", reason="Not in index."
                    )
                if not isinstance(ting_key, str):
                    raise Exception(f"Can't remove ting '{ting}': key not a string.")
                ting_name = ting_key
                remove = None
                for k, v in self._ids.items():
                    if v == ting_key:
                        remove = k
                        break
                if remove:
                    self._ids.pop(remove)

            _ting: Optional[Ting] = self._tings.pop(ting_name, None)
            if _ting is None:
                raise TingException(
                    msg=f"Can't remove Ting with name'{ting_name}'",
                    reason="Not in index.",
                )
        else:
            ting_name = ting.full_name
            if ting_name not in self._tings.keys():
                raise TingException(
                    msg=f"Can't remove Ting with name'{ting_name}'",
                    reason="Not in index.",
                )
            self._tings.pop(ting_name)
            ting_key = None
            for k, v in self._ids.items():
                if v == ting_name:
                    ting_key = k
                    break

            if ting_key is None:
                raise Exception(f"Can't remove ting '{ting_key}'.")
            self._ids.pop(ting_key)
            _ting = ting

        self._tingistry_obj.remove_ting(_ting)
        return _ting

    async def find_all_ids(self) -> AsyncIterator[Hashable]:

        for k in self._ids.keys():
            yield k

        return

    async def _sync(self):

        ids = []
        async for id in self.find_all_ids():

            if id in self._ids.keys():
                yield TingSyncEvent.UPDATED, id
            else:
                yield TingSyncEvent.ADDED, id
            ids.append(id)

        missing = []
        for id in self._ids:
            if id not in ids:
                missing.append(id)

        for m in missing:
            yield TingSyncEvent.REMOVED, m

    async def sync(self):

        async for event_type, id in self._sync():
            try:
                if event_type == TingSyncEvent.ADDED:
                    await self.make_ting(id)
                elif event_type == TingSyncEvent.UPDATED:
                    await self.update_ting(id)
                elif event_type == TingSyncEvent.REMOVED:
                    await self.remove_ting(id)
            except Exception as e:
                log.debug(f"Error with ting event '{event_type}': {e}", exc_info=True)
