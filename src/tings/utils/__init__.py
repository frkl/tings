# -*- coding: utf-8 -*-
from typing import Iterable, Optional

from frkl.common.files.finding import FileMatcher, file_matches
from watchgod import DefaultDirWatcher


def process_ting_name(name: str):

    namespace = None
    if "." in name:
        namespace, name = name.rsplit(".", 1)

    if namespace is None:
        full_name = name
    else:
        full_name = f"{namespace}.{name}"

    return (name, namespace, full_name)


# def process_metadata(name: str, meta: "TingMeta") -> Mapping[str, Any]:
#
#     namespace = None
#     if "." in name:
#         namespace, name = name.rsplit(".", 1)
#
#     meta_ns = meta.namespace
#     if meta_ns and namespace and meta_ns != namespace:
#         raise TingConfigException(
#             msg=f"Can't process entity with name '{name}': {meta}",
#             reason=f"Namespace value provided twice ({namespace} - {meta_ns}).",
#             solution="Use '.'-separated ting-type name, or 'namespace' key in meta properties",
#         )
#     elif meta_ns:
#         namespace = meta_ns
#     # elif namespace and not meta.namespace:
#     #     meta.namespace = namespace
#
#     if namespace:
#         full_name = f"{namespace}.{name}"
#     else:
#         full_name = name
#
#     if namespace is None:
#         namespace = ""
#
#     return {"name": name, "namespace": namespace, "full_name": full_name}


class TingFileWatcher(DefaultDirWatcher):
    def __init__(self, root_path, matchers) -> None:

        self._matchers: Optional[Iterable[FileMatcher]] = matchers
        super(TingFileWatcher, self).__init__(root_path=root_path)

    def should_watch_file(self, entry) -> bool:

        result = file_matches(entry.path, matcher_list=self._matchers)
        return result
