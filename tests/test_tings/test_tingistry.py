# -*- coding: utf-8 -*-
import pytest
from tings.exceptions import TingRegistryException
from tings.ting.proto import ProtoTing
from tings.tingistry import Tingistries, Tingistry


def test_tingistries_singleton():

    t = Tingistries()
    assert t == Tingistries()

    t_br = t.add_tingistry("bring")
    assert "bring" in Tingistries().tingistries.keys()

    assert isinstance(t_br, Tingistry)


def test_add_ting(tingistry):

    ting = tingistry.create_ting("string_ting")

    assert "text" in ting.requires().keys()
    assert "text" in ting.provides().keys()
    assert "transformation" in ting.requires().keys()

    assert ting.full_name in tingistry.tings.keys()

    ting_2 = tingistry.create_ting("string_ting", "test.ting.string_ting")

    assert ting_2.full_name == "test.ting.string_ting"
    assert ting_2.full_name in tingistry.tings.keys()


def test_add_duplicate_ting_name(tingistry):

    ting_1 = tingistry.create_ting("string_ting", "test.duplicate")
    assert ting_1.full_name == "test.duplicate"

    with pytest.raises(TingRegistryException):
        tingistry.create_ting("string_ting", "test.duplicate")

    assert len(tingistry.tings) == 2
    assert ting_1.full_name in tingistry.tings.keys()


def test_get_prototing(tingistry):

    prototing = tingistry.get_ting("string_ting")

    assert issubclass(prototing.__class__, ProtoTing)


def test_prototing_names(tingistry):

    assert "string_ting" in tingistry.prototing_names
