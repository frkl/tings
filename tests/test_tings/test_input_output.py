# -*- coding: utf-8 -*-
import pytest
from tings.defaults import NO_VALUE_MARKER
from tings.ting.common.string import StringTing
from tings.ting.proto import ProtoTing
from tings.tingistry import Tingistry


@pytest.mark.anyio
async def test_ting_set_input_1(string_proto_ting: ProtoTing):

    string_ting: StringTing = string_proto_ting.create_ting_obj("tests.string_ting")

    assert string_ting._value_cache is None
    string_ting.set_input(text="abcd", _ignore_errors=True)
    assert string_ting._value_cache["text"] == NO_VALUE_MARKER

    vals = await string_ting.get_values()
    assert vals == {"text": "abcd"}


@pytest.mark.anyio
async def test_ting_set_input_incl_empty_input_output_maps(tingistry: Tingistry):

    tings_proto_name = tingistry.register_prototing(
        "test.proto.tings", ting_class="string_ting", ting_input={}, ting_output={}
    )
    string_proto_ting = tingistry.get_ting(tings_proto_name)

    string_ting: StringTing = string_proto_ting.create_ting_obj("tests.string_ting")

    assert string_ting._value_cache is None
    string_ting.set_input(text="abcd", _ignore_errors=True)
    assert string_ting._value_cache["text"] == NO_VALUE_MARKER

    vals = await string_ting.get_values()
    assert vals == {"text": "abcd"}


@pytest.mark.anyio
async def test_ting_set_input_incl_input_map_1(tingistry: Tingistry):

    tings_proto_name = tingistry.register_prototing(
        "test.proto.tings",
        ting_class="string_ting",
        ting_input={"text": "a"},
        ting_output={},
    )
    string_proto_ting = tingistry.get_ting(tings_proto_name)

    string_ting: StringTing = string_proto_ting.create_ting_obj("tests.string_ting")

    assert string_ting._value_cache is None
    string_ting.set_input(a="abcd", _ignore_errors=False)

    vals = await string_ting.get_values(raise_exception=True)
    assert vals == {"text": "abcd"}


@pytest.mark.anyio
async def test_ting_set_input_incl_output_map_1(tingistry: Tingistry):

    tings_proto_name = tingistry.register_prototing(
        "test.proto.tings",
        ting_class="string_ting",
        ting_input={},
        ting_output={"text": "a"},
    )
    string_proto_ting = tingistry.get_ting(tings_proto_name)

    string_ting: StringTing = string_proto_ting.create_ting_obj("tests.string_ting")

    assert string_ting._value_cache is None
    string_ting.set_input(text="abcd", _ignore_errors=False)
    # assert string_ting._value_cache["text"] == NO_VALUE_MARKER
    # import pp
    # pp(string_ting.__dict__)

    vals = await string_ting.get_values(raise_exception=True)
    assert vals == {"a": "abcd"}


@pytest.mark.anyio
async def test_ting_set_input_incl_input_output_map_1(tingistry: Tingistry):

    tings_proto_name = tingistry.register_prototing(
        "test.proto.tings",
        ting_class="string_ting",
        ting_input={"text": "a"},
        ting_output={"text": "b"},
    )
    string_proto_ting = tingistry.get_ting(tings_proto_name)

    string_ting: StringTing = string_proto_ting.create_ting_obj("tests.string_ting")

    assert string_ting._value_cache is None
    string_ting.set_input(a="abcd", _ignore_errors=False)
    # assert string_ting._value_cache["text"] == NO_VALUE_MARKER
    # import pp
    # pp(string_ting.__dict__)

    vals = await string_ting.get_values(raise_exception=True)
    assert vals == {"b": "abcd"}
