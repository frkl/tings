# -*- coding: utf-8 -*-
import os

import pytest
from tings.ting.files import TextFile


@pytest.mark.anyio
async def test_file_ting(tingistry, dict_ting_folder):

    text_file_path = os.path.join(dict_ting_folder, "ting_0_1.yaml")

    text_file_ting: TextFile = tingistry.create_ting(
        "text_file", "tests.text_file_ting"
    )

    assert isinstance(text_file_ting, TextFile)

    text_file_ting.set_input(path=text_file_path)

    vals = await text_file_ting.get_values()

    assert vals["size"] > 0
    assert vals["base_name"] == "ting_0_1.yaml"
    assert vals["base_name_no_ext"] == "ting_0_1"
