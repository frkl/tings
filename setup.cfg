# This file is used to configure your project.
# Read more about the various options under:
# http://setuptools.readthedocs.io/en/latest/setuptools.html#configuring-setup-using-setup-cfg-files

[metadata]
name = tings
description = Data to objects.
author = Markus Binsteiner
author-email = markus@frkl.io
license = "The Parity Public License 6.0.0"
url = https://gitlab.com/frkl/tings
long-description = file: README.md
long-description-content-type = text/markdown
# Change if running only on Windows, Mac or Linux (comma-separated)
platforms = any
# Add here all kinds of additional classifiers as defined under
# https://pypi.python.org/pypi?%3Aaction=list_classifiers
classifiers =
    Development Status :: 3 - Alpha
    Programming Language :: Python

[options]
zip_safe = False
packages = find_namespace:
include_package_data = True
package_dir =
    =src

setup_requires =
    setuptools_scm
    setuptools_scm_git_archive

install_requires =
    appdirs>=1.4.4,<2.0.0
    frkl.common[all]
    frkl.args
    frkl.types
    frkl.tasks
    watchgod>=0.5,<1.0
    arrow>=0.15.0,<1.0.0
    pypubsub>=4.0.0,<5.0.0

python_requires = >=3.6

[options.packages.find]
where = src
exclude =
    tests


[options.extras_require]

testing =
    flake8
    mypy
    pytest
    pytest-cov
    tox

develop =
    black
    cruft
    flake8
    importanize
    ipython
    pp-ez
    pre-commit
    watchgod
    wheel
    pip-licenses

docs =
    deepdiff
    formic2
    pydoc-markdown
    mkdocs-simple-hooks
    livereload
    markdown-blockdiag
    markdown-include
    mkdocs-macros-plugin==0.14.6
    mkdocs-material
    pymdown-extensions
    watchgod
    pip-licenses

build =
    frkl.project_meta
    jinja2
    pp-ez



[aliases]
build = bdist_wheel
release = build upload

[bdist_wheel]
# Use this option if your package is pure-python
universal = 1

[devpi:upload]
# Options for the devpi: PyPI server and packaging tool
# VCS export must be deactivated since we are using setuptools-scm
no-vcs = 1
formats = sdist, bdist_wheel

[tool:pytest]
addopts =
    --verbose
norecursedirs =
    dist
    build
    .tox
testpaths = tests


[tox:tox]
envlist = py36, py37, py38, flake8

[testenv]
# uncomment to omit testing package builds & installs for faster runs
# usedevelop = True
setenv =
    PYTHONPATH = {toxinidir}
deps =
    -e{toxinidir}[testing,all]
install_command = pip install --pre --extra-index-url=https://pkgs.frkl.io/frkl/dev --extra-index-url=https://pkgs.frkl.dev/pypi {opts} {packages}
commands =
    pip install -U pip
    py.test --basetemp={envtmpdir}

[testenv:flake8]
basepython = python
deps =
    -e{toxinidir}[testing,all]
    flake8
install_command = pip install --pre --extra-index-url=https://pkgs.frkl.io/frkl/dev --extra-index-url=https://pkgs.frkl.dev/pypi {opts} {packages}
commands = flake8 src

[coverage:run]
branch = True
source = tings
# omit = bad_file.py

[coverage:paths]
source =
    src/
    */site-packages/

[coverage:report]
# Regexes for lines to exclude from consideration
exclude_lines =
    # Have to re-enable the standard pragma
    pragma: no cover

    # Don't complain about missing debug-only code:
    def __repr__
    if self\.debug

    # Don't complain if tests don't hit defensive assertion code:
    raise AssertionError
    raise NotImplementedError

    # Don't complain if non-runnable code isn't run:
    if 0:
    if __name__ == .__main__.:


[flake8]
exclude =
    .tox
    build
    dist
    .eggs
    docs/conf.py
    .git
    __pycache__
ignore = F405, W503, E501
max-line-length = 88

[importanize]
after_imports_new_lines=2
length=88
exclude=
    .tox/*
groups=
    stdlib
    sitepackages
    remainder
    local

[mypy]
python_version = 3.8
mypy_path =
  src/
namespace_packages = false

[mypy-frkl.project_meta.app_environment]
ignore_missing_imports = true

[mypy-appdirs]
ignore_missing_imports = true

[mypy-asyncclick]
ignore_missing_imports = true

[mypy-uvloop]
ignore_missing_imports = true

[mypy-watchgod]
ignore_missing_imports = true

[mypy-pubsub]
ignore_missing_imports = true

[mypy-pubsub.core]
ignore_missing_imports = true

[mypy-pubsub.core.topicargspec]
ignore_missing_imports = true

[mypy-sortedcontainers]
ignore_missing_imports = true

[mypy-arrow]
ignore_missing_imports = true
