# -*- coding: utf-8 -*-
import pytest
from tings.makers.file import TextFileTingMaker
from tings.ting.common.string import StringTing


@pytest.mark.anyio
async def test_file_maker(tingistry, dict_ting_folder):

    print(dict_ting_folder)

    string_proto = tingistry.register_prototing(
        "test.proto.content_string", "string_ting", ting_input={"text": "ting_string"}
    )

    file_ting_maker: TextFileTingMaker = tingistry.create_singleting(
        name="tests.maker",
        ting_class="text_file_ting_maker",
        prototing=string_proto,
        ting_name_strategy="basename_no_ext",
        ting_target_namespace="tests.maker.string_tings",
        file_matchers=[{"type": "extension", "regex": ".*\\.yaml"}],
    )

    file_ting_maker.add_base_paths(dict_ting_folder)
    await file_ting_maker.sync()

    ting_0_1_name = "tests.maker.string_tings.ting_0_1"
    assert ting_0_1_name in tingistry.tings.keys()

    ting_0_1 = tingistry.get_ting(ting_0_1_name)
    assert isinstance(ting_0_1, StringTing)

    vals = await ting_0_1.get_values()

    assert vals == {"text": "a: b\n"}
