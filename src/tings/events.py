# -*- coding: utf-8 -*-
from enum import Enum
from typing import (
    TYPE_CHECKING,
    Any,
    Callable,
    List,
    Mapping,
    Optional,
    Sequence,
    Tuple,
    Union,
)

from frkl.common.exceptions import FrklException
from pubsub import pub
from pubsub.core import ITopicDefnProvider
from pubsub.core.topicargspec import ArgSpecGiven


if TYPE_CHECKING:
    from tings.ting import Ting
    from tings.tingistry import Tingistry  # noqa
    from tings.ting.proto import ProtoTing  # noqa


class TingTopicProvider(ITopicDefnProvider):
    def __init__(self):

        self._default_arg_spec = ArgSpecGiven(
            argsDocs={
                "event_name": "The name of the event",
                "source": "The source (Ting) object.",
                "event_details": "A dict containing details about the event.",
            },
            reqdArgs=("event_name", "source", "event_details"),
            # reqdArgs=()
        )

    def getDefn(self, topicNameTuple: Sequence[str]) -> Tuple[str, ArgSpecGiven]:

        return (f"{topicNameTuple[0]} even topic", self._default_arg_spec)

    def topicNames(self) -> List[str]:

        return []

    def getTreeDoc(self) -> str:

        return "treedoc"


class TingsEvent(Enum):

    TING_INVALIDATED = "invalidated"
    TING_VALUES_CHANGED = "values_changed"
    TING_INPUT_CHANGED = "input_changed"

    TING_CREATED = "ting_created"
    TING_REMOVED = "ting_removed"

    CHILD_ADDED = "child_added"
    CHILD_REMOVED = "child_removed"
    CHILD_UPDATED = "child_updated"
    CHILDS_CHANGED = "childs_changed"


class TingPublishCentral(object):
    def __init__(self):
        pass

    def get_topic(self, source: "Ting", event_type: TingsEvent):

        return f"{source.subscription_topic}.events.{event_type.value}"

    def publish_event(
        self,
        source: "Ting",
        event_type: TingsEvent,
        event_details: Optional[Mapping[str, Any]] = None,
    ):

        if event_details is None:
            event_details = {}

        topic = self.get_topic(source, event_type)

        pub.sendMessage(
            topic,
            event_name=event_type.value,
            source=source,
            event_details=event_details,
        )

    def subscribe_to_prototing(
        self,
        handler: Callable,
        tingistry: Union[str, "Tingistry"],
        prototing: Union[str, "ProtoTing"],
        *event_types: TingsEvent,
    ):

        if not isinstance(tingistry, str):
            tingistry = tingistry.full_name
        if not isinstance(prototing, str):
            prototing = prototing.full_name

        if not event_types:
            topic = f"{tingistry}.tings.{prototing}.events"
            pub.subscribe(handler, topic)
            return

        for event_type in event_types:
            topic = f"{tingistry}.tings.{prototing}.events.{event_type.value}"

            pub.subscribe(handler, topic)

    def unsubscribe_from_prototing(
        self,
        handler,
        tingistry: Union[str, "Tingistry"],
        prototing: Union[str, "ProtoTing"],
        *event_types: TingsEvent,
    ):

        if not isinstance(tingistry, str):
            tingistry = tingistry.full_name
        if not isinstance(prototing, str):
            prototing = prototing.full_name

        if not event_types:
            topic = f"{tingistry}.{prototing}.events"
            pub.unsubscribe(handler, topic)
            return

        for event_type in event_types:
            topic = f"{tingistry}.{prototing}.events.{event_type.value}"
            pub.unsubscribe(handler, topic)

    def subscribe_to_ting(
        self, handler: Callable, target: "Ting", *event_types: TingsEvent
    ):

        from tings.ting.proto import ProtoTing  # noqa
        from tings.ting.tings import Tings  # noqa

        if not event_types:
            topic: str = target.subscription_topic
            pub.subscribe(handler, topic)
            return

        for event_type in event_types:
            topic = self.get_topic(target, event_type=event_type)

            if event_type in [
                TingsEvent.TING_INVALIDATED,
                TingsEvent.TING_INPUT_CHANGED,
                TingsEvent.TING_VALUES_CHANGED,
            ]:
                pub.subscribe(handler, topic)
            elif event_type in [
                TingsEvent.CHILD_ADDED,
                TingsEvent.CHILD_REMOVED,
                TingsEvent.CHILD_UPDATED,
                TingsEvent.CHILDS_CHANGED,
            ]:

                if not issubclass(target.__class__, Tings):
                    raise FrklException(
                        msg=f"Can't subscribe to event type '{event_type.value}' on Ting '{target.full_name}'.",
                        reason="Ting does not sub-class the 'Tings' class.",
                    )

                pub.subscribe(handler, topic)

            elif event_type in [TingsEvent.TING_CREATED, TingsEvent.TING_REMOVED]:

                if not issubclass(target.__class__, (ProtoTing)):
                    raise FrklException(
                        msg=f"Can't subscribe to event type '{event_type.value}' on Ting '{target.full_name}'.",
                        reason="Ting does not sub-class the 'Tingistry' or 'ProtoTing' class.",
                    )

                pub.subscribe(handler, topic)

            else:
                raise NotImplementedError()

    def unsubscribe_from_ting(
        self, handler: Callable, target: "Ting", *event_types: TingsEvent
    ):

        from tings.tingistry import Tingistry  # noqa
        from tings.ting.proto import ProtoTing  # noqa
        from tings.ting.tings import Tings

        if not event_types:
            topic: str = target.subscription_topic
            pub.unsubscribe(handler, topic)
            return

        for event_type in event_types:
            topic = self.get_topic(target, event_type=event_type)

            if event_type in [
                TingsEvent.TING_INVALIDATED,
                TingsEvent.TING_INPUT_CHANGED,
                TingsEvent.TING_VALUES_CHANGED,
            ]:
                pub.unsubscribe(handler, topic)
            elif event_type in [
                TingsEvent.CHILD_ADDED,
                TingsEvent.CHILD_REMOVED,
                TingsEvent.CHILD_UPDATED,
                TingsEvent.CHILDS_CHANGED,
            ]:

                if not issubclass(target.__class__, Tings):
                    raise FrklException(
                        msg=f"Can't subscribe to event type '{event_type.value}' on Ting '{target.full_name}'.",
                        reason="Ting does not sub-class the 'Tings' class.",
                    )

                pub.unsubscribe(handler, topic)

            else:

                if not issubclass(target.__class__, (Tingistry, ProtoTing)):
                    raise FrklException(
                        msg=f"Can't subscribe to event type '{event_type.value}' on Ting '{target.full_name}'.",
                        reason="Ting does not sub-class the 'Tingistry' or 'ProtoTing' class.",
                    )

                pub.unsubscribe(handler, topic)


#
# class TingsEvent(Enum):
#
#     TING_CREATED = (
#         "ting_created",
#         "events.created",
#         "ting",
#         "ting",
#         {},
#     )  # type: ignore
#     TING_INVALIDATED = (
#         "ting_invalidated",
#         "events.invalidated",
#         "ting",
#         "ting",
#         {},
#     )  # type: ignore
#     TING_VALUES_CHANGED = (
#         "values_changed",
#         "events.values_changed",
#         "ting",
#         "current_values",
#         {"changed_values": "dict"},
#     )  # type: ignore
#     TING_INPUT_CHANGED = (
#         "input_changed",
#         "events.input_changed",
#         "ting",
#         "ting",
#         {},
#     )  # type: ignore
#
#     def __new__(
#         cls,
#         event_name: str,
#         topic_subpath: str,
#         source_type: str,
#         subject_type: str,
#         event_details_schema: Dict[str, str],
#     ) -> "TingsEvent":
#         obj = object.__new__(cls)
#         obj._value_ = event_name
#         obj.topic_subpath = topic_subpath
#         obj.source_type = source_type
#         obj.subject_type = subject_type
#         obj.event_details_schema = event_details_schema
#         return obj
#
#     def topic_path(self, ting: Union[str, "Ting"], extra_path: str = None) -> str:
#
#         if isinstance(ting, str):
#             t_name = ting
#         else:
#             t_name = ting.full_name
#
#         topic = f"ting.{t_name}.{self.topic_subpath}"  # type: ignore
#         if extra_path:
#             topic = topic + f".{extra_path}"
#         return topic
#
#     def send(
#         self,
#         source_obj: "Ting",
#         subject_obj,
#         event_details: Dict = None,
#         extra_path: str = None,
#     ) -> None:
#
#         if event_details is None:
#             event_details = {}
#
#         topic = self.topic_path(source_obj, extra_path=extra_path)
#         pub.sendMessage(
#             topic,
#             event_name=self.value,
#             source=source_obj,
#             subject=subject_obj,
#             event_details=event_details,
#         )
#
#     def subscribe(
#         self, func: Callable, ting: Union[str, "Ting"], extra_path: str = None
#     ) -> None:
#
#         topic = self.topic_path(ting, extra_path=extra_path)
#         pub.subscribe(func, topic)
#
#     def unsubscribe(
#         self, func: Callable, ting: Union[str, "Ting"], extra_path: str = None
#     ) -> None:
#
#         topic = self.topic_path(ting, extra_path=extra_path)
#         pub.unsubscribe(func, topic)
#
#
# class TingsEvent(Enum):
#
#     TINGS_CHILD_ADDED = (
#         "child_added",
#         "events.childs.added",
#         "tings",
#         "childs",
#         {"added_child": "ting"},
#     )
#     TINGS_CHILD_REMOVED = (
#         "child_removed",
#         "events.childs.removed",
#         "tings",
#         "childs",
#         {"removed_child": "ting"},
#     )
#     TINGS_CHILD_UPDATED = (
#         "child_updated",
#         "events.childs.updated",
#         "tings",
#         "childs",
#         {"updated_child": "ting"},
#     )
#     TINGS_CHILDS_CHANGED = (
#         "childs_changed",
#         "events.childs.changed",
#         "tings",
#         "childs",
#         {
#             TINGS_CHILD_ADDED[0]: "list",
#             TINGS_CHILD_REMOVED[0]: "list",
#             TINGS_CHILD_UPDATED[0]: "list",
#         },
#     )
#
#     def __new__(
#         cls,
#         event_name: str,
#         topic_subpath: str,
#         source_type: str,
#         subject_type: str,
#         event_details_schema: Dict[str, str],
#     ):
#         obj = object.__new__(cls)
#         obj._value_ = event_name
#         obj.topic_subpath = topic_subpath
#         obj.source_type = source_type
#         obj.subject_type = subject_type
#         obj.event_details_schema = event_details_schema
#         return obj
#
#     def topic_path(self, tings: Union[str, "Tings"], extra_path: str = None) -> str:
#
#         if isinstance(tings, str):
#             t_name = tings
#         else:
#             t_name = tings.full_name
#
#         topic = f"tings.{t_name}.{self.topic_subpath}"  # type: ignore
#         if extra_path:
#             topic = topic + f".{extra_path}"
#         return topic
#
#     def send(
#         self,
#         source_obj: "Tings",
#         subject_obj,
#         event_details: Dict = None,
#         extra_path: str = None,
#     ) -> None:
#
#         if event_details is None:
#             event_details = {}
#
#         pub.sendMessage(
#             self.topic_path(source_obj, extra_path=extra_path),
#             event_name=self.value,
#             source=source_obj,
#             subject=subject_obj,
#             event_details=event_details,
#         )
#
#     def subscribe(
#         self, func: Callable, tings: Union[str, "Tings"], extra_path: str = None
#     ) -> None:
#
#         pub.subscribe(func, self.topic_path(tings, extra_path=extra_path))
#
#     def unsubscribe(
#         self, func: Callable, tings: Union[str, "Tings"], extra_path: str = None
#     ) -> None:
#
#         topic = self.topic_path(tings, extra_path=extra_path)
#         pub.unsubscribe(func, topic)
#
#
# class TingistryEvent(Enum):
#
#     TING_CREATED = ("ting_created", "tings.events.created", "tingistry", "ting", {})
#     TING_REMOVED = ("ting_removed", "tings.events.removed", "tingistry", "ting", {})
#
#     def __new__(
#         cls,
#         event_name: str,
#         topic_subpath: str,
#         source_type: "Tingistry",
#         subject_type: Any,
#         event_details_schema: Dict[str, Any],
#     ):
#         obj = object.__new__(cls)
#         obj._value_ = event_name
#         obj.topic_subpath = topic_subpath
#         obj.source_type = source_type
#         obj.subject_type = subject_type
#         obj.event_details_schema = event_details_schema
#         return obj
#
#     def topic_path(self, tingistry: Union[str, "Tingistry"], extra_path: str = None):
#
#         if isinstance(tingistry, str):
#             t_name = tingistry
#         else:
#             t_name = tingistry.full_name
#
#         topic = f"tingistry.{t_name}.{self.topic_subpath}"  # type: ignore
#         if extra_path:
#             topic = topic + f".{extra_path}"
#         return topic
#
#     def send(
#         self,
#         source_obj: Union[str, "Tingistry"],
#         subject_obj,
#         event_details: Dict = None,
#         extra_path: str = None,
#     ):
#
#         if event_details is None:
#             event_details = {}
#
#         topic = self.topic_path(source_obj, extra_path=extra_path)
#
#         pub.sendMessage(
#             topicName=topic,
#             event_name=self.value,
#             source=source_obj,
#             subject=subject_obj,
#             event_details=event_details,
#         )
#
#     def subscribe(
#         self, func: Callable, tingistry: Union[str, "Tingistry"], extra_path: str = None
#     ):
#
#         pub.subscribe(func, self.topic_path(tingistry, extra_path=extra_path))
#
#     def unsubscribe(
#         self, func: Callable, tingistry: Union[str, "Tingistry"], extra_path: str = None
#     ):
#
#         topic = self.topic_path(tingistry, extra_path=extra_path)
#         pub.unsubscribe(func, topic)
