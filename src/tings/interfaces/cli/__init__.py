# -*- coding: utf-8 -*-
import asyncio
import os
from pathlib import Path

import asyncclick as click
from frkl.common.cli.exceptions import handle_exc_async
from frkl.common.cli.logging import logzero_option_async
from frkl.common.formats.auto import AutoInput
from pubsub import pub
from tings.tingistry import Tingistries


click.anyio_backend = "asyncio"

# flake8: noqa


@click.group()
@logzero_option_async()
@click.pass_context
async def cli(ctx):
    pass


#
# @cli.command()
# @click.pass_context
# async def watch(ctx):
#     from pubsub import pub
#
#     def ting_listener(source, event_type, event_details, topic=pub.AUTO_TOPIC):
#
#         # import pp
#         # pp(f"topic: {topic.getName()} - {event_type} - {event_details}")
#         import asyncio
#
#         loop = asyncio.get_event_loop()
#         print(topic.getName())
#         task = loop.create_task(source.get_values())
#
#         def wrap(future):
#             try:
#                 print("----")
#                 print(event_type)
#                 if event_type == "values_changed":
#                     print(future.result())
#             except (Exception) as e:
#                 print(e)
#
#         task.add_done_callback(wrap)
#
#     # pub.subscribe(ting_listener, "tings")
#     # pub.subscribe(snoop, pub.ALL_TOPICS)
#
#     pub.subscribe(ting_listener, "ting")
#
#     path = "/home/markus/projects/tings/tings/examples/freckwork"
#
#     tingistry = Tingistries().add_tingistry("test")
#
#     tingistry.register_ting_type(
#         "test", "ting-ting", ting_types=["file-details", "dict-ting"]
#     )
#
#     tings = SeedTings(name="testtings", ting_type="test", ting_namespace="test")
#
#     matchers = [{"type": "extension", "regex": ".frecklet$"}]
#     file_source = FileSource(name="source", base_paths=path, matchers=matchers)
#     tings.add_ting_source(file_source)
#
#     tasks = await tings.watch(wait=False)
#
#     await wait_for_tasks_and_user_keypress(
#         tasks=tasks, msg="Watching sources for tings objecct, press 'q' to exit."
#     )

TEST_TYPES = [
    {
        "name": "freckles.frecklet_dict",
        "ting_class": "dict",
        "ting_init": {"key_map": {"args": "dict", "frecklets": "dict"}},
    },
    {
        "name": "freckles.frecklets",
        "ting_class": "tings",
        "ting_init": {
            "ting_type": "freckles.frecklet_dict",
            "child_name_strategy": "basename_no_ext",
        },
    },
    {
        "name": "freckles.frecklet_input",
        "ting_class": "ting_ting",
        "ting_init": {"ting_types": ["text_file", "dict"]},
    },
    {
        "name": "freckles.frecklet_file_watcher",
        "ting_class": "file_watch_source",
        "ting_init": {"matchers": [{"type": "extension", "regex": ".frecklet"}]},
    },
    {
        "name": "freckles.frecklet_file_source",
        "ting_class": "ting_watch_source",
        "ting_init": {
            "source_ting_type": "freckles.frecklet_file_watcher",
            "seed_ting_type": "freckles.frecklet_input",
        },
    },
    {"name": "freckles.dict_source", "ting_class": "dict_source"},
]


@cli.command(name="test")
@click.pass_context
@handle_exc_async
async def test(ctx):

    tingistry = Tingistries().add_tingistry("test")
    tingistry.register_prototing("test1.test2.test3", "text_file")

    prototing = tingistry.get_ting("test1.test2.test3")
    print(prototing)

    ting = tingistry.create_ting("test1.test2.test3")
    print(ting)
    ting = tingistry.create_ting("test1.test2.test3")
    print(ting)
    ting = tingistry.create_ting("test1.test2.test3")
    print(ting)
    # pp(tingistry.__dict__)

    singleting = tingistry.create_singleting("sing.le.ting", "text_file")
    print(singleting)


@cli.command(name="export")
@click.pass_context
@handle_exc_async
async def export_data(ctx):

    path = Path("/tmp/dump.json")

    tt = Tingistries()

    tingistry = tt.add_tingistry("ting_test", prototings=TEST_TYPES)

    tings = tingistry.create_ting("freckles.frecklets", "frecklets")
    source = tingistry.create_ting("freckles.frecklet_file_source", "frecklet_source")

    def update(*args, topic=pub.AUTO_TOPIC, **kwargs):

        print(topic)

    pub.subscribe(update, f"frecklets.events")
    source.add_source_items(os.getcwd())

    source.set_tings(tings)
    await source.sync()
    write_path = "/home/markus/projects/tings/bring/repos/frecklets_json"
    for name, child in tings.childs.items():
        print(name)
        dict_val = await child.get_values("dict")
        # print(list(dict_val["dict"].keys()))

        # val = serialize(dict_val, format="json", target={"target": os.path.join(write_path, f"{child.name}.frecklet")})
        # print(val)

        # print(val)
    # print(source.export_seeds(output_format="json", target={"target": "/tmp/dump.json", "target_opts": {"force": True}}))


@cli.command(name="watch")
@click.pass_context
@handle_exc_async
async def watch(ctx):

    tt = Tingistries()

    tingistry = tt.add_tingistry("ting_test", prototings=TEST_TYPES)

    tings = tingistry.create_ting("freckles.frecklets", "frecklets")
    source = tingistry.create_ting("freckles.frecklet_file_source", "frecklet_source")

    def update(*args, topic=pub.AUTO_TOPIC, **kwargs):

        print(topic)
        print(list(tings.childs.keys()))

    pub.subscribe(update, f"frecklets.events")
    source.add_source_items(os.getcwd())

    source.set_tings(tings)

    await source.watch()


@cli.command(name="import")
@click.pass_context
@handle_exc_async
async def import_data(ctx):

    # from tings.sources.files import FileWatchSource
    # fws = FileWatchSource()
    # print("XXX")
    # print(fws)

    path = Path("/tmp/dump.json")
    json_data = AutoInput(path).content()

    tt = Tingistries()

    tingistry = tt.add_tingistry("ting_test", prototings=TEST_TYPES)

    tings = tingistry.create_ting("freckles.frecklets", "frecklets")
    source = tingistry.create_ting("freckles.dict_source", "frecklet_source")
    source.add_source_items(json_data)

    source.set_tings(tings)
    await source.sync()

    for name, child in tings.childs.items():
        print(name)
        val = await child.get_values("dict")
        # print(val["dict"])
        # print(val)


@cli.command(name="sub")
@click.pass_context
@handle_exc_async
async def sub(ctx):

    ctx.obj = {}

    tt = Tingistries(preload_modules="bring.bring")

    tt.add_tingistry("bringistry", tingistry_class="bringistry")
    val = await tt.get_values()
    # bringistry: Bring = val["tingistries"]["bringistry"]


@cli.command(name="tings")
@click.pass_context
@handle_exc_async
async def test_tings(ctx):
    ctx.obj = {}

    tt = Tingistries(preload_modules="bring.bring")

    tt.add_tingistry("bringistry", tingistry_class="bringistry")
    val = await tt.get_values()
    # bringistry: Bring = val["tingistries"]["bringistry"]
    # bringistry.set_source(
    #     source_type="bring.bring_file_source", base_paths=[os.getcwd()]
    # )
    # bringistry.source.source.add_source_items(os.getcwd())

    # path = "/home/markus/projects/tings/bring/repos/simple/"
    # bringistry._pkg_source.seeds._add_seed(seed_id=path, seed={"path": path})
    # await bringistry._pkg_source.sync()
    # pp(await bringistry._pkg_source.get_values())

    # bringistry._pkg_source.source.add_base_path(path)

    # nodes = bringistry.tings

    def event_happended(source, event_details, topic=pub.AUTO_TOPIC):

        # print("event_type: {}".format(topic.getName()))
        # print(source)
        print(topic.getName())
        # print(event_details)
        # import pp
        # pp(source.__dict__)
        # print(source.current_values)
        # if not hasattr(source, "_childs"):
        #     return

        # pp(list(source._childs.keys()))

        # print("=---")
        # for child in source._childs.values():
        #     print(child.current_values)

        # bringistry._tings_tree.show()

        async def wrap():
            childs = await source.get_values()
            print(childs)
            # for k, v in childs["childs"].items():
            #     print("XXXXX")
            #     print(k)
            #     val = await v.get_values()
            #     pp(val)
            # pp(bringistry._tings_tree.nodes.keys())
            # bringistry._tings_tree.show(nid="bring.bring_pkgs")
            # for n, c in childs["childs"].items():
            #      print(await c.get_values())

        asyncio.create_task(wrap())

    # pub.subscribe(event_happended, "bring.bring_pkgs.events")

    # await bringistry._pkg_source.watch()
    # await bringistry._pkg_source.sync()

    # pp(bringistry._bring_pkgs)
    # export_data = await bringistry._bring_pkgs.export()

    # dump = Path("/tmp/dump.json")
    # with open(dump, 'w') as f:
    #     json.dump(export_data, f)

    # content = bringistry._pkg_source.export_seeds(
    #     output_format="json",
    #     target={"target": "/tmp/dump.json", "target_opts": {"force": True}},
    # )
    # print(content)


if __name__ == "__main__":
    cli()
