# -*- coding: utf-8 -*-
from typing import Any, Mapping, Union

from tings.ting import SimpleTing


class StringTing(SimpleTing):
    def provides(self) -> Mapping[str, Union[str, Mapping[str, Any]]]:

        return {"text": "string"}

    def requires(self) -> Mapping[str, Union[str, Mapping[str, Any]]]:

        return {"text": "string", "transformation": "dict?"}

    async def retrieve(self, *value_names: str, **requirements) -> Mapping[str, Any]:
        result = {}

        if "text" in value_names:
            result["text"] = requirements["text"]

        return result
