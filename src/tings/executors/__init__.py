# -*- coding: utf-8 -*-
import logging
from abc import ABCMeta, abstractmethod
from typing import TYPE_CHECKING, Any, Dict


if TYPE_CHECKING:
    from tings.ting import TingTask

log = logging.getLogger("tings")


class TingExecutor(metaclass=ABCMeta):
    def __init__(self) -> None:

        pass

    @abstractmethod
    def execute(self, ting_value: "TingTask", raise_exception=False) -> Dict[str, Any]:

        pass
