# -*- coding: utf-8 -*-
import logging
import re
from pathlib import Path
from typing import Any, Mapping, Optional, Union

from frkl.common.formats import CONTENT_TYPE
from frkl.common.formats.auto import auto_parse_string
from frkl.common.jinja_templating import (
    create_jinja_environment,
    get_template_keys,
    process_string_template,
)
from frkl.common.jinja_templating.filters import ALL_FRTLS_FILTERS
from tings.makers.file import TextFileTingMaker
from tings.ting import SimpleTing, TingMeta
from tings.ting.tings import SubscripTings
from tings.tingistry import Tingistry


FRONTMATTER_REGEX = r"^([-\s]*---[-\s]*\n)*(.*?)(\n[-\s]*---[-\s]*)(.*)"

TEMPLATE_JINJA_ENV = create_jinja_environment(delimiter_profile="frkl")
for filter_name, filter_details in ALL_FRTLS_FILTERS.items():
    TEMPLATE_JINJA_ENV.filters[filter_name] = filter_details["func"]

log = logging.getLogger("tings")


class TemplaTing(SimpleTing):
    def __init__(self, name: str, meta: TingMeta) -> None:

        super().__init__(name=name, meta=meta)

    def requires(self) -> Mapping[str, Any]:

        return {"ting_string": "string"}

    def provides(self) -> Mapping[str, Any]:

        return {
            "args": "dict",
            "full_content": "string",
            "frontmatter": "dict",
            "content": "string",
        }

    async def retrieve(self, *value_names: str, **requirements) -> Mapping[str, Any]:

        result = {}
        full_content = requirements["ting_string"]
        if "full_content" in value_names:
            result["full_content"] = full_content

        if (
            "frontmatter" in value_names
            or "content" in value_names
            or "args" in value_names
        ):
            matches = re.match(FRONTMATTER_REGEX, full_content, re.DOTALL)
            try:
                frontmatter_string = matches.group(2)  # type: ignore
                if frontmatter_string:
                    frontmatter = auto_parse_string(
                        frontmatter_string, content_type=CONTENT_TYPE.yaml
                    )
                else:
                    frontmatter = {}
            except Exception as e:
                log.debug(
                    f"Could not parse frontmatter tempting for '{self.full_name}': {e}",
                    exc_info=True,
                )
                frontmatter = {}
            result["frontmatter"] = frontmatter

            try:
                content = matches.group(4)  # type: ignore
            except Exception as e:
                log.debug(
                    f"Could not parse content for tempting '{self.full_name}': {e}",
                    exc_info=True,
                )
                content = ""
            result["content"] = content

        if "args" in value_names:

            args = result["frontmatter"].get("args", {})
            template_keys = get_template_keys(
                result["content"], jinja_env=TEMPLATE_JINJA_ENV
            )
            args_dict = {}
            for tk in template_keys:
                if tk in args.keys():
                    args_dict[tk] = args[tk]
                else:
                    args_dict[tk] = {}
            result["args"] = args_dict

        return result

    async def process_template(self, **vars: Any) -> str:

        values: Mapping[str, Any] = await self.get_values(
            "content", resolve=True
        )  # type: ignore
        content = values["content"]

        repl_dict = {}
        for k, v in vars.items():
            if v is not None:
                repl_dict[k] = v

        result = process_string_template(
            content, replacement_dict=repl_dict, jinja_env=TEMPLATE_JINJA_ENV
        )

        return result


class TemplaTingRepo(SimpleTing):
    def __init__(self, name: str, meta: TingMeta) -> None:

        if meta is None:
            raise Exception("'meta' value not set, this is a bug")
        self._tingistry_obj: Tingistry = meta.tingistry

        super().__init__(name=name, meta=meta)

        self._templates: Optional[SubscripTings] = None

        self._template_ting_maker: TextFileTingMaker = self._tingistry_obj.create_singleting(  # type: ignore
            name=f"{self.full_name}.maker",
            ting_class="text_file_ting_maker",
            prototing="templa_ting",
            ting_name_strategy="basename_no_ext",
            ting_target_namespace=f"{self.full_name}.temptings",
            file_matchers=[{"type": "extension", "regex": ".*\\.tempting"}],
        )

    def requires(self) -> Mapping[str, Union[str, Mapping[str, Any]]]:

        return {}

    def provides(self) -> Mapping[str, Union[str, Mapping[str, Any]]]:

        return {"temptings": "dict"}

    async def retrieve(self, *value_names: str, **requirements) -> Mapping[str, Any]:

        temptings = await self.get_temptings()
        return {"temptings": temptings}

    def add_repo_path(self, repo_path: Union[str, Path]):

        self._template_ting_maker.add_base_paths(repo_path)

    async def get_temptings(self, update: bool = False) -> Mapping[str, TemplaTing]:

        if self._templates is None:
            self._templates = self._tingistry_obj.create_singleting(  # type: ignore
                name=f"{self.full_name}.temptings",
                ting_class="subscrip_tings",
                subscription_namespace=f"{self.full_name}.temptings",
                prototing="templa_ting",
            )
            await self._template_ting_maker.sync()
        elif update:
            await self._template_ting_maker.sync()

        temptings: Mapping[str, TemplaTing] = {
            k.split(".")[-1]: v  # type: ignore
            for k, v in self._templates.childs.items()  # type: ignore
        }  # type: ignore

        return temptings
