# -*- coding: utf-8 -*-
import copy
import logging
from typing import Any, Dict, Iterable, List, Mapping, Optional, Type, Union

from frkl.args.hive import ArgHive
from frkl.common.formats.auto import AutoInput
from frkl.common.strings import find_free_name
from frkl.common.types import isinstance_or_subclass, load_modules
from frkl.types.typistry import Typistry
from pubsub import pub
from tings.defaults import TINGS_DEFAULT_MODULES
from tings.events import TingTopicProvider, TingsEvent
from tings.exceptions import TingRegistryException
from tings.ting import Ting, TingMeta, TingTask
from tings.ting.proto import ProtoTing, ProtoTingBuilder, ProtoTings, SingleTing
from tings.utils import process_ting_name


log = logging.getLogger("tings")

pub.setTopicUnspecifiedFatal(True)
pub.addTopicDefnProvider(TingTopicProvider())

load_modules(TINGS_DEFAULT_MODULES)


class Tingistry(Ting):
    def __init__(
        self,
        name: str,
        meta: Optional[TingMeta] = None,
        prototings: Optional[Iterable[Mapping]] = None,
        tings: Optional[Iterable[Mapping]] = None,
        modules: Optional[Iterable[str]] = None,
        classes: Optional[Iterable[Union[Type, str]]] = None,
        typistry: Optional[Typistry] = None,
    ):

        _modules = copy.copy(TINGS_DEFAULT_MODULES)
        if modules:
            _modules.extend(modules)
        load_modules(_modules)

        _classes: List[Union[str, Type]] = [Ting]
        if classes:
            _classes.extend(classes)

        self._init_modules = _modules
        self._init_classes = _classes

        self._typistry: Optional[Typistry] = typistry
        self._typistry_augmented = False
        self._arg_hive: ArgHive = ArgHive(typistry=self._typistry)

        self._ting_classes: Optional[Dict[str, Dict[str, Any]]] = None

        if not prototings:
            prototings = []
        else:
            prototings = list(prototings)

        if not tings:
            tings = []
        else:
            tings = list(tings)
        self._init_prototings: List[Mapping] = prototings
        self._init_tings: List[Mapping] = tings

        self._prototings: Optional[Dict[str, Dict[str, Any]]] = None
        self._tings: Optional[Dict[str, Ting]] = None

        # _meta["info"] = {"tingistry": "tingistries", "prototing": "tingistry"}
        if meta is None:
            meta = TingMeta(tingistry=self, prototing_name="tingistry")

        super().__init__(name=name, meta=meta)

        if "." in self.full_name:
            raise TingRegistryException(
                msg=f"Can't create Tingistry with name '{self.full_name}'.",
                reason="Non-root-level names for Tingistries not supported (yet).",
            )

    def _init_tings_and_modules(self):

        self._ting_classes = {}
        self._prototings = {}
        self._tings = {}

        # for ting_cls in self.typistry.get_subclasses(Ting):
        #
        #     self._register_ting_class(ting_class=ting_cls)

        for k, v in self._ting_classes.items():

            self.register_prototing(prototing_name=v["name"], ting_class=k)

        if self._init_prototings:
            for ting_type in self._init_prototings:
                self.register_prototing(**ting_type)

        if self._init_tings:
            for ting in self._init_tings:
                self.create_ting(
                    prototing=ting["prototing"], ting_name=ting["ting_name"]
                )

        # self._root_task = Tasks(open_ended=True, background_task=True)
        # self._root_task.task_desc.name = self.name

    @property
    def arg_hive(self) -> ArgHive:
        return self._arg_hive

    @property
    def typistry(self) -> Typistry:

        if self._typistry is None:

            self._typistry = Typistry()

        if not self._typistry_augmented:
            self._typistry.add_module_paths(*self._init_modules)
            self._typistry.add_classes(*self._init_classes)

            self._typistry_augmented = True

        return self._typistry

    def add_module_paths(self, *modules):

        if self._typistry is None:
            self._init_modules.extend(modules)
        else:
            self.typistry.add_module_paths(*modules)

    def add_classes(self, *classes):

        if self._typistry is None:
            self._init_classes.extend(classes)
        else:
            self.typistry.add_classes(*classes)

    @property
    def tings(self) -> Mapping[str, Ting]:

        if self._tings is not None:
            return self._tings

        self._init_tings_and_modules()
        return self._tings  # type: ignore

    @property
    def ting_names(self) -> Iterable[str]:

        return sorted(self.tings.keys())

    @property
    def prototings(self) -> Dict[str, Dict[str, Any]]:

        if self._prototings is not None:
            return self._prototings

        self._init_tings_and_modules()

        return self.prototings

    def _register_ting_class(self, ting_class: Type):

        if isinstance(ting_class, type) and issubclass(ting_class, Ting):
            type_cls = ting_class
        else:
            raise TypeError(
                f"Invalid 'ting_type' type '{ting_class}': needs to be 'str' or subclass of Ting."
            )

        self.typistry.register_classes(ting_class)

        if hasattr(ting_class, "_ting_type_name"):
            type_name = ting_class._ting_type_name  # type: ignore
        elif hasattr(ting_class, "_plugin_name"):
            type_name = ting_class._plugin_name  # type: ignore
        else:
            type_name = self.typistry.get_id(ting_class)

        name, namespace, full_name = process_ting_name(type_name)

        if self._ting_classes is None:
            raise Exception(
                "Internal '_ting_classes' attribute not initialized. This is a bug."
            )

        self._ting_classes[full_name] = {
            "class": type_cls,
            "full_name": full_name,
            "name": name,
            "namespace": namespace,
        }

        log.debug(
            f"tingistry '{self.full_name}': registered ting class: {type_cls} as '{full_name}'"
        )

        self.invalidate()

    @property
    def ting_classes(self) -> Dict[str, Dict[str, Any]]:

        if self._ting_classes is not None:
            return self._ting_classes

        self._init_tings_and_modules()

        if self._ting_classes is None:
            raise Exception(
                "Internal '_ting_classes' attribute not initialized. This is a bug."
            )

        return self._ting_classes

    def get_ting_class_details(self, type_id: Union[str, Type]) -> Dict[str, Any]:

        if isinstance(type_id, str):

            details = self.ting_classes.get(type_id, None)

        elif isinstance(type_id, type):

            details = None

            for k, v in self.ting_classes.items():
                if v["class"] == type_id:
                    details = v

        else:
            raise TypeError(f"Invalid ting class: {type(type_id)}")

        if not details:
            if self._ting_classes is None:
                raise TingRegistryException(
                    msg="Can't retrieve ting class details.",
                    reason="Ting classes not loaded yet. This is a bug.",
                )
            avail: Iterable[str] = sorted(self._ting_classes.keys())
            avail_str = "\n  - ".join(avail)
            raise TingRegistryException(
                msg=f"Could not retrieve ting class details for '{type_id}'",
                reason="No matching Ting class registered in Tingistry.",
                solution=f"Register the desired class, or use one of the existing ones:\n{avail_str}",
            )

        return details

    def create_singleting(
        self,
        name: str,
        ting_class: Union[str, Type],
        ting_meta: Optional[Mapping[str, Any]] = None,
        ting_input: Optional[Mapping[str, str]] = None,
        ting_output: Optional[Mapping[str, str]] = None,
        prototing_namespace: Optional[str] = None,
        **ting_init: Any,
    ) -> Ting:

        if prototing_namespace is None:
            prototing_namespace = "internal.singletings"

        if "ting_name" in ting_init:
            raise TingRegistryException(
                msg=f"Can't create singleting '{name}'.",
                reason="Argument 'ting_name' provided, this is not allowed.",
            )

        _args = dict(ting_init)
        _args["ting_name"] = name

        prototing_name = f"{prototing_namespace}.{name}"
        self.register_prototing(
            prototing_name=prototing_name,
            ting_class=ting_class,
            ting_meta=ting_meta,
            ting_input=ting_input,
            ting_output=ting_output,
            prototing_factory="singleting",
            **_args,
        )

        ting = self.get_ting(name)
        if ting is None:
            raise Exception(f"Can't retrieve singleting '{name}', this is a bug.")
        # ting = self.create_ting(prototing=type_name, ting_name=name)

        return ting

    def register_prototing(
        self,
        prototing_name: str,
        ting_class: Union[str, Type],
        ting_input: Optional[Mapping[str, str]] = None,
        ting_output: Optional[Mapping[str, str]] = None,
        prototing_factory: str = "default",
        ting_meta: Optional[Mapping[str, Any]] = None,
        **ting_init: Any,
    ) -> str:
        """Register a prototing.

        Check the 'ProtoTing' class for more details, but basically, a 'ProtoTing' is a registered template that can produce
        "Ting' objects of a certain kind and specification.

        Args:
            - *prototing_name*: the (full) name of the ProtoTing, to be used as identifier when creating Ting objects
            - *ting_class*: a class of class_name, pointing to the base class the resulting Ting objects will be based on
            - *ting_input*: an optional map to change the name of the arguments a Ting object takes (keys of the 'requires' attribute)
            - *ting_output*: an optional map to change the keys of the result map of a Ting object (keys of the 'provides' attribute)
            - *prototing_factory*: which factory to use when creating the Ting object (options: 'default', 'singleting')
            - *ting_meta*: optional key/value pairs which go into the TingMeta class of the resulting ProtoTing

        Returns:
            the name the ProtoTing is registered under

        """

        _name, _namespace, _full_name = process_ting_name(prototing_name)

        existing = self.prototings.get(_full_name, None)

        if existing:
            raise TingRegistryException(
                msg=f"Can't register prototing '{_full_name}'",
                reason="Prototing with that name already exists.",
            )

        if isinstance(ting_class, type):

            self._register_ting_class(ting_class=ting_class)

        self.prototings[_full_name] = {
            "init_data": {
                "prototing_name": _name,
                "prototing_namespace": _namespace,
                "ting_class": ting_class,
                "ting_meta": ting_meta,
                "ting_input": ting_input,
                "ting_output": ting_output,
            },
            "prototing_factory": prototing_factory,
        }

        for k, v in ting_init.items():
            self.prototings[_full_name]["init_data"][k] = v

        # special case, for those we assume a ting always exists once the prototing is registered
        if prototing_factory == "singleting":
            self.get_ting(_full_name)
            # ting.create_ting_obj("bring.contexts")
            # import pp
            # pp(ting.__dict__)

        return _full_name

    @property
    def prototing_names(self) -> List[str]:

        return sorted(list(self.prototings.keys()))

    def get_ting(self, name, raise_exception=False) -> Optional[Ting]:

        if name in self.tings.keys():

            ting = self.tings.get(name)
            return ting

        return self.get_prototing(name=name, raise_exception=raise_exception)

    def get_prototing(self, name: str, raise_exception=False) -> Optional[ProtoTing]:

        if name not in self.prototings.keys():

            if raise_exception:
                raise TingRegistryException(
                    msg=f"Can't retrieve Ting '{name}'.", reason="No such ting exists."
                )
            return None

        init_data = self.prototings[name]["init_data"]

        # init_data["meta"]["info"] = {
        #     "tingistry": self.full_name,
        #     "prototing": "prototing",
        # }

        factory = self.prototings[name]["prototing_factory"]

        if factory == "default":
            try:
                proto_ting: ProtoTing = ProtoTings(tingistry=self, **init_data)
            except Exception as e:
                raise TingRegistryException(
                    msg=f"Can't create ProtoTings '{name}'", parent=e
                )
        elif factory == "singleting":
            try:
                if "ting_name" not in init_data.keys():
                    raise TingRegistryException(
                        msg=f"Can't create singleting '{name}'.",
                        reason="No 'ting_name' argument provided.",
                    )
                proto_ting = SingleTing(tingistry=self, **init_data)
            except Exception as e:
                raise TingRegistryException(
                    msg=f"Can't create ProtoTings '{name}'", parent=e
                )
        else:
            _builder: Optional[ProtoTingBuilder] = self.get_ting(  # type: ignore
                factory
            )
            if _builder is None:
                raise TingRegistryException(
                    msg=f"Can't create ProtoTing '{name}'.",
                    reason=f"No factory with the name '{factory}' registered.",
                )

            if not isinstance_or_subclass(_builder, ProtoTingBuilder):
                raise TingRegistryException(
                    msg=f"Can't create ProtoTing '{name}'.",
                    reason=f"Factory with name '{factory}' does not sub-class ProtoTingBuilder",
                )

            try:
                proto_ting = _builder.build_proto_ting(tingistry=self, **init_data)
            except Exception as e:
                raise TingRegistryException(
                    msg=f"Can't create ProtoTings '{name}'", parent=e
                )

        if self._tings is None:
            self.tings
        self._tings[name] = proto_ting  # type: ignore

        proto_ting.subscribe_to(self._ting_created, TingsEvent.TING_CREATED)
        proto_ting.subscribe_to(self._ting_removed, TingsEvent.TING_REMOVED)

        if (
            factory == "singleting"
            and hasattr(proto_ting, "_ting_name")
            and proto_ting._ting_name  # type: ignore
        ):  # type: ignore
            self.create_ting(proto_ting, proto_ting._ting_name)  # type: ignore

        return proto_ting

    def _ting_created(
        self, event_name: str, source: Ting, event_details: Dict[str, Any]
    ):

        ting = event_details["ting"]

        if ting.full_name in self.tings.keys():
            raise TingRegistryException(
                msg=f"Can't register newly created Ting '{ting.full_name}'.",
                reason="A Ting with that same name already exists.",
            )

        self._tings[ting.full_name] = ting  # type: ignore

    def _ting_removed(
        self, event_name: str, source: Ting, event_details: Dict[str, Any]
    ):

        ting = event_details["ting"]

        if ting.full_name not in self.tings.keys():
            raise TingRegistryException(
                msg=f"Can't un-register Ting '{ting.full_name}'.",
                reason="No Ting with that name is registered.",
            )

    def create_ting(
        self,
        prototing: Union[str, ProtoTing],
        ting_name: str = None,
        publish: bool = True,
    ) -> Ting:

        if ting_name in self.tings.keys():
            raise TingRegistryException(
                msg=f"Can't create ting '{ting_name}'.",
                reason="Ting with that name already exists in tingistry.",
            )

        if isinstance(prototing, str):

            _proto_ting: ProtoTing = self.get_ting(prototing)  # type: ignore
            if _proto_ting is None:
                raise TingRegistryException(
                    msg=f"Can't create ting '{ting_name}'.",
                    reason=f"No ProtoTing '{prototing}' exists. Available ProtoTings: {', '.join(self.prototing_names)}",
                )
        else:
            _proto_ting = prototing

        if not isinstance_or_subclass(_proto_ting, ProtoTing):
            raise TingRegistryException(
                msg=f"Can't create ting '{ting_name}'.",
                reason=f"Invalid type for ProtoTing obj: {type(_proto_ting)}",
            )

        if _proto_ting.tingistry is None:
            raise Exception("Can't create ting: No tingistry available.")

        if _proto_ting.tingistry.full_name != self.full_name:
            raise TingRegistryException(
                msg=f"Can't create ting '{ting_name}'.",
                reason=f"Provided ProtoTing not available in this tingistry ({self.full_name}): {_proto_ting.tingistry.full_name}",
            )

        ting = _proto_ting.create_ting_obj(ting_name=ting_name, publish=publish)
        self._tings[ting.full_name] = ting  # type: ignore

        return ting

    def find_free_ting_name(self, namespace: str, name: str) -> str:

        existing = set()
        full_name = f"{namespace}.{name}"
        for n in self.tings.keys():
            if not n.startswith(full_name):
                continue

            extra = n[len(full_name) :].split(".")[0]  # noqa
            existing.add(extra)

        ting_name = find_free_name(
            name, existing, method="count", method_args={"try_no_postfix": False}
        )
        return ting_name

    def remove_ting(self, ting_name: Union[str, Ting]) -> Ting:

        if isinstance(ting_name, Ting):
            ting_name = ting_name.full_name

        ting = self.get_ting(ting_name)
        if ting is None:
            raise ValueError(
                f"Can't remove Ting '{ting_name}': no Ting with that name exists"
            )

        if self._tings is None:
            raise Exception("'tings' attribute not set yet, this is a bug")
        self._tings.pop(ting.full_name)

        if ting.meta.is_singleting:
            self.remove_ting(ting.prototing)

        self.publish_event(TingsEvent.TING_REMOVED, ting=ting)

        return ting

    def requires(self) -> Dict[str, str]:

        return {}
        # return {
        #     "ting_types": "dict",
        #     "ting_descs": "[dict]"
        # }

    def _assemble_retrieval_tasks(self, *value_names: str) -> "TingTask":

        if not value_names:
            _value_names: Iterable[str] = self.provides().keys()
        else:
            _value_names = value_names

        values_old = copy.copy(self._calculate_current())

        tt = TingTask(ting=self, old_values=values_old)
        result: Dict[str, Any] = {}
        if "prototings" in _value_names:
            result["prototings"] = self.prototings
        if "tings" in _value_names:
            result["tings"] = self.tings

        tt.set_result(result=result)

        return tt

    def provides(self) -> Dict[str, str]:

        return {"prototings": "dict", "tings": "dict"}

    def get_ting_wrap(self, register_path: str) -> Type:

        raise NotImplementedError()
        # node = self._ting_wrap_tree.get_node(register_path)
        # if node is None:
        #     return None
        #
        # return node.data

    # @property
    # def create_ting_wrap_class(
    #     self,
    #     prototing: str,
    #     cls_name: str = None,
    #     base_class: Type = None,
    #     register_path: str = None,
    #     create_constructor: bool = True,
    # ) -> Type:
    #
    #     if cls_name is None:
    #         cls_name = prototing
    #
    #     if register_path is None:
    #         register_path = prototing
    #
    #     from ting_wrap import TingWrap
    #
    #     metacls_args = {
    #         "prototing": prototing,
    #         "tingistry": self,
    #         "create_constructor": create_constructor,
    #     }
    #
    #     if base_class:
    #         new_class = TingWrap(cls_name, (base_class,), {}, **metacls_args)
    #     else:
    #         new_class = TingWrap(cls_name, (object,), {}, **metacls_args)
    #
    #     node = self._ting_wrap_tree.get_node(cls_name)
    #     if node is not None and node.data is not None:
    #
    #         raise TingRegistryException(
    #             msg=f"Can't add newly created ting wrap '{cls_name}'",
    #             reason="Name already taken",
    #         )
    #     add_node_data_to_tree(self._ting_wrap_tree, path=register_path, obj=new_class)
    #     return new_class
    #
    # def wrap_ting(self, ting: Ting):
    #
    #     ting_type = ting.prototing
    #
    #     wrap_cls = self.get_ting_wrap(ting_type)
    #     if wrap_cls is None:
    #         wrap_cls = self.create_ting_wrap_class(ting_type)
    #
    #     wrapped = wrap_cls(_ting=ting)
    #     return wrapped


class Tingistries(Tingistry):

    _instance: Optional[Tingistry] = None
    tingistries: Dict[str, Tingistry] = {}

    @classmethod
    def create_from_dict(
        cls, input_dict: Union[str, Mapping], typistry: Optional[Typistry] = None
    ):

        if not isinstance(input_dict, Mapping):
            smart_input = AutoInput(input_value=input_dict)
            _input_mapping: Mapping[str, Any] = smart_input.get_content()
        else:
            _input_mapping = input_dict

        return cls.create(
            name=_input_mapping["name"],
            prototings=_input_mapping.get("prototings", None),
            tings=_input_mapping.get("tings", None),
            tingistry_class=_input_mapping.get("tingistry_class", None),
            modules=_input_mapping.get("modules", None),
            classes=_input_mapping.get("classes", None),
            typistry=typistry,
            **_input_mapping.get("tingistry_init", {}),
        )

    @classmethod
    def create(
        cls,
        name: str,
        prototings: List[Dict] = None,
        tings: List[Dict] = None,
        tingistry_class: Union[str, Type] = None,
        modules: List[str] = None,
        classes: List[Union[str, Type]] = None,
        typistry: Optional[Typistry] = None,
        **tingistry_init: Any,
    ) -> Tingistry:

        tingistries: Tingistries = cls()
        t = tingistries.add_tingistry(
            name=name,
            prototings=prototings,
            tings=tings,
            tingistry_class=tingistry_class,
            modules=modules,
            classes=classes,
            typistry=typistry,
            **tingistry_init,
        )
        return t

    def __new__(cls, *args, **kwargs):

        if Tingistries._instance is None:
            # meta = TingMeta(tingistry=None, prototing_name="tingistry")  # type: ignore
            Tingistries._instance = Tingistry.__new__(cls)
            # Tingistries._instance.__init__()
            super(Tingistries, cls._instance).__init__("tingistries", meta=None)

        return Tingistries._instance

    def __init__(self):
        pass

    def _assemble_retrieval_tasks(self, *value_names: str) -> "TingTask":

        if not value_names:
            _value_names: Iterable[str] = "tingistries"
        else:
            _value_names = value_names

        if _value_names != ("tingistries"):
            raise NotImplementedError()

        values_old = copy.copy(self._calculate_current())

        tt = TingTask(ting=self, old_values=values_old)
        tt.set_result(result={"tingistries": self.tingistries})

        return tt

    def requires(self) -> Dict[str, str]:

        return {"config": "dict"}

    def provides(self) -> Dict[str, str]:

        return {"tingistries": "dict"}

    def get_tingistry(self, name=None) -> Tingistry:

        if name is None:
            name = "default"

        if name == "default" and "default" not in self.tingistries.keys():
            self.add_tingistry("default")

        if name not in self.tingistries.keys():
            raise KeyError(
                f"No tingistry with name '{name}', available: {', '.join(self.tingistries.keys())}"
            )

        return self.tingistries[name]

    def add_tingistry(
        self,
        name: str,
        prototings: List[Dict] = None,
        tings: List[Dict] = None,
        tingistry_class: Optional[Union[str, Type]] = None,
        modules: List[str] = None,
        classes: List[Union[str, Type]] = None,
        typistry: Optional[Typistry] = None,
        **tingistry_init: Any,
    ) -> Tingistry:

        if tingistry_class:
            if not isinstance(tingistry_class, (str, type)):
                raise TypeError(
                    f"Can't create tingistry, invalid tingistry_class type '{type(tingistry_class)}': {tingistry_class}"
                )
            if isinstance(tingistry_class, str):
                _tingistry_class: Type = self.typistry.get_class(tingistry_class)
            else:
                _tingistry_class = tingistry_class

            if _tingistry_class != Tingistry and not issubclass(
                _tingistry_class, Tingistry
            ):
                raise TypeError(
                    f"Can't create tingistry, invalid tingistry_class type '{type(tingistry_class)}', must be Tingistry or subclass of Tingistry."
                )

            t_cls = _tingistry_class
        else:
            t_cls = Tingistry

        tingistry = t_cls(
            name,
            prototings=prototings,
            tings=tings,
            modules=modules,
            classes=classes,
            typistry=typistry,
            **tingistry_init,
        )

        self.register_tingistry(tingistry)

        return tingistry

    def register_tingistry(self, tingistry: Tingistry):

        if not isinstance_or_subclass(tingistry, Tingistry):
            raise TypeError(f"Not a tingistry or tingistry subclass: {tingistry}")

        if tingistry.full_name in self.tingistries.keys():
            raise ValueError(
                f"Can't register tingistry '{tingistry.full_name}', Tingistry with that name already exists."
            )

        self.tingistries[tingistry.full_name] = tingistry
        self.invalidate()

    def remove_tingistry(self, name: str) -> Optional[Tingistry]:

        tingistry = self.tingistries.pop(name, None)
        return tingistry

    def remove_all_tingistries(self) -> Iterable[Tingistry]:
        result: List[Tingistry] = []
        for t in self.tingistries.values():
            result.append(t)

        self.tingistries.clear()
        return result
