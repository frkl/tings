# -*- coding: utf-8 -*-
from tings.ting.tings import Tings


def test_create_tings(tingistry):

    tings_proto_name = tingistry.register_prototing(
        "test.proto.tings", ting_class="tings", prototing="string_ting"
    )
    tings_proto_ting = tingistry.get_ting(tings_proto_name)

    assert not tings_proto_ting.childs

    tings = tingistry.create_ting("test.proto.tings", "test.tings")

    assert "test.tings" in tings_proto_ting.childs.keys()
    assert isinstance(tings, Tings)


def test_add_to_tings(tingistry):

    tings_proto_name = tingistry.register_prototing(
        "test.proto.tings", ting_class="tings", prototing="string_ting"
    )
    # tings_proto_ting = tingistry.get_ting(tings_proto_name)
    tings: Tings = tingistry.create_ting(tings_proto_name, "test.tings")

    string_ting_proto = tingistry.register_prototing(
        "test.proto.string", ting_class="string_ting"
    )

    string_ting = tingistry.create_ting(string_ting_proto, "test.string_ting")
    string_ting2 = tingistry.create_ting(string_ting_proto, "test.string_ting2")

    tings.add_child(string_ting)

    assert len(tings.childs) == 1

    tings.add_child(string_ting2)
    assert len(tings.childs) == 2
