# -*- coding: utf-8 -*-
import copy
import logging
from abc import ABCMeta, abstractmethod
from functools import total_ordering
from typing import (
    TYPE_CHECKING,
    Any,
    Callable,
    Dict,
    Iterable,
    List,
    Mapping,
    MutableMapping,
    Optional,
    Tuple,
    Union,
)

from anyio import create_lock, create_task_group
from anyio.abc import Lock
from frkl.args.arg import RecordArg
from frkl.args.exceptions import ArgValidationError
from frkl.common.async_utils import wrap_async_task
from frkl.common.exceptions import FrklException, FrklMultiParentException
from frkl.common.types import isinstance_or_subclass
from tings.defaults import NO_VALUE_MARKER
from tings.events import TingPublishCentral, TingsEvent
from tings.exceptions import (
    TingConfigException,
    TingException,
    TingTaskException,
    TingValueError,
)
from tings.executors import TingExecutor
from tings.utils import process_ting_name


if TYPE_CHECKING:
    from tings.tingistry import Tingistry
    from tings.ting.proto import ProtoTing

log = logging.getLogger("tings")


class TingMeta(object):
    """Class to hold metadata about a ProtoTing."""

    def __init__(
        self,
        tingistry: "Tingistry",
        prototing_name: str,
        input: Optional[Mapping[str, str]] = None,
        output: Optional[Mapping[str, str]] = None,
        labels: Optional[Mapping[str, str]] = None,
        is_singleting: bool = False,
        hidden: bool = False,
        auto_created: bool = False,
        defaults: Optional[Mapping[str, Any]] = None,
        input_ting_type: Optional[str] = None,
        **meta: Any,
    ):

        self._tingistry: "Tingistry" = tingistry
        self._prototing_name: str = prototing_name

        if input is None:
            input = {}
        self._input_map: Mapping[str, str] = input
        if output is None:
            output = {}
        self._output_map: Mapping[str, str] = output

        self._is_singleting: bool = is_singleting
        self._hidden: bool = hidden
        self._auto_created: bool = auto_created
        if labels is None:
            labels = {}
        self._labels: Mapping[str, str] = labels
        if defaults is None:
            defaults = {}
        self._defaults: MutableMapping[str, Any] = dict(defaults)
        self._meta: MutableMapping[str, Any] = meta

        self._input_args: Optional[RecordArg] = None
        self._output_args: Optional[RecordArg] = None

        self._requires_args: Optional[RecordArg] = None

        if input_ting_type is None:
            input_ting_type = "dict_input_ting"
        self._input_ting_type = input_ting_type

    def create_input_ting(self, parent_ting_name: str):

        input_meta = TingMeta(
            tingistry=self._tingistry, prototing_name=self._input_ting_type, hidden=True
        )

        if self._input_ting_type == "dict_input_ting":

            input_ting: InputTing = DictInputTing(
                name=f"{parent_ting_name}_input", meta=input_meta
            )
            return input_ting

    @property
    def requires_args(self) -> RecordArg:

        if self._requires_args is not None:
            return self._requires_args

        ref_ting = self.prototing.get_reference_ting()
        self._requires_args = self._tingistry.arg_hive.create_record_arg(
            ref_ting.requires()
        )

        return self._requires_args

    def get_input_key(self, orig_requires_key: str) -> str:
        """Return the translated (or not) name of an input key."""

        return self._input_map.get(orig_requires_key, orig_requires_key)

    def get_output_key(self, orig_provides_key: str) -> str:

        return self._output_map.get(orig_provides_key, orig_provides_key)

    def translate_input_to_required(self, input_dict) -> Dict[str, Any]:

        if not self._input_map:
            return dict(input_dict)

        result: Dict[str, Any] = {}
        for k, v in input_dict.items():
            match = False
            for ko, vo in self._input_map.items():
                if vo == k:
                    result[ko] = v
                    match = True
                    break
            if not match:
                result[k] = v

        return result

    def translate_required_to_input(self, required_dict) -> Dict[str, Any]:

        if not self._input_map:
            return dict(required_dict)

        result: Dict[str, Any] = {}
        for k, v in required_dict.items():

            new_key = self._input_map.get(k, k)
            result[new_key] = v

        return result

    def translate_required_names_to_input(self, *value_names: str) -> Iterable[str]:

        result = []

        for vn in value_names:
            result.append(self._input_map.get(vn, vn))
        return result

    @property
    def input_args(self) -> RecordArg:

        if self._input_args is not None:
            return self._input_args

        ref_ting = self.prototing.get_reference_ting()
        ref_ting_requires = ref_ting.requires()
        translated = self.translate_required_to_input(ref_ting_requires)
        self._input_args = self._tingistry.arg_hive.create_record_arg(translated)

        return self._input_args

    @property
    def output_args(self) -> RecordArg:

        if self._output_args is not None:
            return self._output_args

        ref_ting = self.prototing.get_reference_ting()
        ref_ting_provides = ref_ting.provides()
        translated = self.translate_provides_to_output(ref_ting_provides)
        self._output_args = self._tingistry.arg_hive.create_record_arg(translated)

        return self._output_args

    def translate_provides_names_to_output(self, *value_names: str) -> Iterable[str]:

        result = []

        for vn in value_names:
            result.append(self._output_map.get(vn, vn))
        return result

    def translate_output_names_to_provides(self, *value_names: str) -> Iterable[str]:

        result = []
        for vn in value_names:

            match = False
            for ko, vo in self._output_map.items():
                if vo == vn:
                    result.append(ko)
                    match = True
                    break
            if match is False:
                result.append(vn)

        return result

    def translate_provides_to_output(
        self, provides_dict: Mapping[str, Any]
    ) -> Dict[str, Any]:

        if not self._output_map:
            return dict(provides_dict)

        result: Dict[str, Any] = {}
        for k, v in provides_dict.items():
            new_key = self._output_map.get(k, k)
            result[new_key] = v

        return result

    def translate_output_to_provides(
        self, output_dict: Mapping[str, Any]
    ) -> Dict[str, Any]:

        if not self._output_map:
            return dict(output_dict)

        result: Dict[str, Any] = {}
        for k, v in output_dict.items():
            match = False
            for ko, vo in self._output_map.items():
                if vo == k:
                    result[ko] = v
                    match = True
                    break
            if not match:
                result[k] = v

        return result

    @property
    def tingistry(self):
        return self._tingistry

    @property
    def meta(self) -> Mapping[str, Any]:
        return self._meta

    @property
    def prototing_name(self) -> str:
        return self._prototing_name

    @property
    def prototing(self) -> "ProtoTing":
        return self._tingistry.get_ting(self._prototing_name)  # type: ignore

    @property
    def input_map(self) -> Mapping[str, str]:

        return self._input_map

    @property
    def output_map(self) -> Mapping[str, str]:
        return self._output_map

    # @property
    # def namespace(self) -> Optional[str]:
    #     return self._namespace
    #
    # @namespace.setter
    # def namespace(self, namespace: str) -> None:
    #     self._namespace = namespace

    @property
    def hidden(self) -> bool:
        return self._hidden

    @property
    def is_singleting(self) -> bool:
        return self._is_singleting

    @property
    def labels(self) -> Mapping[str, str]:
        return self._labels

    @property
    def defaults(self) -> Mapping[str, Any]:

        return self._defaults

    def set_default(self, key: str, value: Any) -> None:

        self._defaults[key] = value

    @property
    def auto_created(self) -> bool:
        return self._auto_created


@total_ordering  # type: ignore
class Ting(metaclass=ABCMeta):

    event_central = TingPublishCentral()

    def __init__(self, name: str, meta: TingMeta, **kwargs):

        _name, _namespace, _full_name = process_ting_name(name)

        self._name: str = _name
        """The base name of this Ting."""
        self._namespace: Optional[str] = _namespace
        """The (optional) namespace of this Ting."""
        self._full_name: str = _full_name
        """The fully qualified name of this Ting."""
        self._meta: TingMeta = meta
        """Prototing-related metadata for this category of Tings."""
        self._tingistry: str = self._meta.tingistry.name
        """The tingistry this Ting lives within."""

        self._update_event_conf: Dict[str, Any] = {"disabled": False}
        """Configuration for event publishing for this Ting."""
        # dict_merge(
        #     self._update_event_conf, self._meta.get("publish", {}), copy_dct=False
        # )

        self._always_retrieve_values: bool = False
        """Whether to always retrieve values instantly if the input changes for this Ting."""

        self._subscription_topic: str = f"{self._tingistry}.tings.{self.full_name}"
        """The subscription topic for this Ting."""

        self._requirements: Optional[Dict[str, Ting]] = None
        """A map of all requirement Tings that this Ting uses input from. Key is the key from 'requires', value is
        the requirement Ting."""
        self._requirement_map: Optional[Dict[Ting, List[str]]] = None
        """A map that outlines which requirement Tings are responsible for which of this Tings input keys."""
        self._upstream: Dict[str, List[Ting]] = {}
        """A map of upstream Tings that need to be notified when a value changes in this Ting."""

        self._value_cache: Optional[Dict[str, Any]] = None
        """The value cache, used to prevent (expensive) re-calculations of output values of this Ting."""

        # Every Ting has a 'child' Ting that holds input values that are not served by any of the added requirement Tings.
        if not isinstance_or_subclass(self, InputTing):

            self._input_ting: InputTing = self._meta.create_input_ting(self._full_name)
            self._input_ting.set_provides(self.requires())
            self._requirement_tings: List[Ting] = []

            self.set_requirements()

        # TODO: check lock behaviour
        self._get_value_lock: Optional[Lock] = None

    @property
    def name(self) -> str:
        """The base name of this Ting."""
        return self._name

    @property
    def namespace(self) -> Optional[str]:
        """The (optional) namespace of this Ting."""
        return self._namespace

    @property
    def full_name(self) -> str:
        """The fully qualified name of this Ting."""
        return self._full_name

    @property
    def prototing(self) -> str:
        """The registered name of the prototing for this Ting."""
        return self.meta.prototing_name

    def publish_event(self, event_type: TingsEvent, **event_details: Any):
        """Publish an event related to this Ting."""

        Ting.event_central.publish_event(self, event_type, event_details)

    def subscribe_to(self, handler: Callable, *event_types: TingsEvent):
        """Subscribe a handler to events from this Ting."""

        Ting.event_central.subscribe_to_ting(handler, self, *event_types)

    def unsubscribe_from(self, handler: Callable, *event_types: TingsEvent):
        """Unsubscribe a handler to events from this Ting."""

        Ting.event_central.unsubscribe_from_ting(handler, self, *event_types)

    @property
    def meta(self) -> TingMeta:
        """Prototing-related metadata for the class/instance-category of this Ting."""

        return self._meta

    @abstractmethod
    def provides(self) -> Mapping[str, Union[str, Mapping[str, Any]]]:
        """A map of input values this Ting relies on.

        This returns a dictionary with the name of the input arguments as keys, and either the name or metadata of the
        type of the argument as value.

        TODO: documentation of argument metadata

        This can be empty. If the prototing contains an input_map dictionary, all or some of the argument names might be
        named different on user-facing interfaces.
        """

        pass

    def requires(self) -> Mapping[str, Union[str, Mapping[str, Any]]]:
        """A map of input values this Ting relies on.

        This returns a dictionary with the name of the output values as keys, and either the name or metadata of the
        type of the output as value.

        TODO: documentation of argument metadata

        This can be empty, but that is only useful for very special cases. If the prototing contains an output_map dictionary,
        all or some of the value names might be named different on user-facing interfaces.
        """

        return {}

    # @property
    # def is_singleting(self) -> bool:
    #     return self.meta.is_singleting

    @property
    def tingistry(self) -> "Tingistry":
        """The Tingistry object under which this Ting lives."""

        return self.meta.tingistry

    @property
    def subscription_topic(self) -> str:
        """The subscription topic where events relating to this Ting will be published."""

        return self._subscription_topic

    def _calculate_current(
        self,
        include_errors: bool = True,
        include_no_value: bool = True,
        include_extra_values: bool = False,
    ) -> Dict[str, Any]:
        """Return all (cached) values, using the original, ting-native keys (not the one in the output_map).

        Args:
            - *include_errors*: whether to include values that did not resolve successfully
            - *include_no_value: whether to include values that are not resolved yet
            - *include_extra_values*: include internal extra values that are stored for caching or other purposes (mostly useful for development)
        """

        if self._value_cache is None:
            self._value_cache = {}

        for k, v in self.provides().items():

            if k not in self._value_cache.keys():
                self._value_cache[k] = NO_VALUE_MARKER

        result = {}
        for k, v in self._value_cache.items():

            if not include_extra_values and k not in self.provides().keys():
                continue

            if v == NO_VALUE_MARKER:
                if not include_no_value:
                    continue

            if include_errors or (
                not isinstance(v, TingValueError)
                and not issubclass(v.__class__, TingValueError)
            ):
                result[k] = v

        return result

    def _set_value_cache(self, value_cache: Dict[str, Any]) -> None:
        """Set the value cache dictionary."""

        self._value_cache = value_cache

    @property
    def output_args(self) -> RecordArg:

        return self.meta.output_args

    @property
    def requirement_map(self) -> Dict["Ting", List[str]]:
        """Return a map of Tings that are required by this Ting, and a list of values they provide as input."""

        if self._requirement_map is not None:
            return self._requirement_map

        if self._requirements is None:
            raise Exception(
                f"No requirements set for ting {self.full_name}, this is most likely a bug."
            )

        self._requirement_map = {}
        missing = []
        for k, v in self.requires().items():
            req_ting = self._requirements.get(k, None)
            if req_ting is None:
                m = self.meta.get_input_key(k)
                missing.append(m)
            else:
                self._requirement_map.setdefault(req_ting, []).append(k)

        if missing:
            raise TingException(
                ting=self,
                msg=f"Error processing ting '{self.full_name}'.",
                reason=f"No value(s) for requirement keys: {', '.join(missing)}",
            )

        return self._requirement_map

    @abstractmethod
    def _assemble_retrieval_tasks(self, *value_names: str) -> "TingTask":
        """Return a 'TingTask' object that wraps all the tasks that are necessary to resolve the requested values.

        Usually one would inherit from 'SimpleTing', in which case one can ignore the implementation of this. Otherwise,
        check out that implementation for reference.

        Args:
            - *value_names*: a list of names for the values that should be resolved
        """
        pass

    async def get_value(self, value_name: str, raise_exception: bool = False) -> Any:
        """Return the resolved value for the provided name.

        Args:
            value_name: the name of the value
            raise_exception: whether to raise an exception on failure

        Returns:
            The value, or an object of the class TingValueError (or a subclass thereof)
        """

        value: Mapping[str, Any] = await self.get_values(  # type: ignore
            value_name, resolve=True, raise_exception=raise_exception
        )  # type: ignore
        return value[value_name]

    async def get_values(
        self,
        *value_names: str,
        resolve: Union[bool, TingExecutor] = True,
        raise_exception: bool = False,
    ) -> Union[Mapping[str, Any], "TingTask"]:
        """The core method in a Ting, retrieves the required values it encapsulates.

        If 'resolve' is set to 'False', this method returns a DAG of tasks that need to be executed to get the values instead.
        This is useful to use executor plugins to use other threads, processes, or even remote machines to do the
        computations. If set to True, the internal default executor is used. Alternatively, a TingExecutor instance can be provided.

        For now, there exists no implementatio of a TingExecutor, so it's recommended to always use the internal one. Well, really,
        there's no choice apart from implementing one.

        Args:
            - *value_names*: the names of the value that need to be calculated
            - *resolve*: whether to calculate the values directly, or return a list of tasks that are required for computation
        """

        if not value_names:
            translated_value_names: Iterable[str] = self.provides().keys()
        else:
            translated_value_names = self.meta.translate_output_names_to_provides(
                *value_names
            )

        ting_task = self._assemble_retrieval_tasks(*translated_value_names)

        if not resolve:
            return ting_task

        if ting_task.ready:
            return ting_task.result

        if resolve is True:
            # TODO: this is a bit silly, need to figure out a better place to do this
            if self._get_value_lock is None:
                self._get_value_lock = create_lock()

            # async with self._get_value_lock:
            result = await ting_task.run_in_place(raise_exception=raise_exception)
        elif issubclass(resolve.__class__, TingExecutor):
            result = resolve.execute(
                ting_task, raise_exception=raise_exception
            )  # type: ignore
        else:
            raise Exception("No result, this is most likely a bug.")

        return result

    def update_value_cache(self, updates: Dict[str, Any]) -> None:

        self._calculate_current()
        self._value_cache.update(updates)  # type: ignore

    def _set_result(self, result: Dict[str, Any]) -> Dict[str, Any]:
        """Set the current value of this ting.

        Be sure to use the 'native' keys of this thing (the one returned by 'provides', not 'output'.

        Returns:
            the current calculated value of this Ting, incl. previously cached ones
        """

        # TODO: validate result against schema
        old_values = copy.copy(self._calculate_current())

        changed = {}

        for k, v in result.items():

            if k not in self.provides().keys():
                continue

            translated_key = self.meta.get_output_key(k)

            v_old = old_values.get(k, NO_VALUE_MARKER)
            if v != v_old:
                changed[translated_key] = {"old": v_old, "new": v}

        if changed:
            self.update_value_cache(result)

            self.publish_event(
                TingsEvent.TING_VALUES_CHANGED,
                changed=changed,
                current=self._calculate_current(),
            )

            for k, upstream_list in self._upstream.items():
                if k in changed.keys():
                    for upstream in upstream_list:
                        upstream.input_changed()

        current = self._calculate_current()
        return current

    def set_requirements(self, *requirements: "Ting") -> None:
        """Set one or several SeedTings that will be used to read required values from.

        Args:
            - *requirements*: one or several SeedTings this Ting depends upon
        """

        if isinstance_or_subclass(self, InputTing):
            return

        self._requirement_tings.extend(requirements)

        self._requirements = {}
        self._requirement_map = None

        req: Ting
        for req in self._requirement_tings:

            if not issubclass(req.__class__, Ting):
                raise TypeError(f"Requirement not a 'Ting' object: {type(req)}")

            for k, v in self.requires().items():

                translated_key = self.meta.get_input_key(k)

                if translated_key in req.output_args.arg_names:
                    # TODO: check/compare schemas
                    if k in self._requirements.keys():
                        raise TingConfigException(
                            ting=self,
                            msg=f"Can't add requirement to ting '{self.full_name}': {req}",
                            reason=f"Requirement key '{k}' is already connected to ting: {self._requirements[k].full_name}",
                            solution="Check the dependency tree.",
                        )

                    self._requirements[k] = req
                    req._add_upstream(self, translated_key)

        missing = {}
        for k, v in self.requires().items():

            translated_key = self.meta.get_input_key(k)

            if k not in self._requirements.keys():
                missing[translated_key] = v
                self._requirements[k] = self._input_ting

        self._input_ting.set_provides(provides=missing)
        for k in self._input_ting.provides().keys():
            self._input_ting._add_upstream(self, k)

    def _requirement_changed(self, source_ting: "Ting", *value_names: str) -> None:

        self.invalidate()

    def invalidate(self) -> None:

        old = copy.copy(self._calculate_current())
        self._value_cache = None
        self._invalidate()

        vc = self._calculate_current()
        changed = {}
        for k in self.provides().keys():
            if old[k] == vc[k]:
                continue
            changed[k] = {"old": old[k], "new": vc[k]}

        if not changed:
            return

        self.publish_event(TingsEvent.TING_INVALIDATED)

        if changed:
            self.publish_event(
                TingsEvent.TING_VALUES_CHANGED, current=vc, changed=changed
            )

        to_notify: Dict[Ting, List[str]] = {}

        for key, ti in self._upstream.items():
            for ting in ti:
                upstream_key = ting.meta.get_input_key(key)
                to_notify.setdefault(ting, []).append(upstream_key)

        for ting, value_names in to_notify.items():
            # TODO: check value names
            ting._requirement_changed(self, *value_names)

    def _invalidate(self) -> None:
        """Can be overwritten if some special invalidate tasks need to be run in a Ting-child-class."""

        pass

    # async def export(self, *additional_values: str) -> Dict[str, Dict[str, Any]]:
    #     """Export this Ting as a dictionary.
    #
    #     By default, only 'required' values will be exported (under the 'required' key in the result dict), as the other ones can be re-calculated using
    #     the schema of this Ting.
    #
    #     If 'additional_values' are specified, those are exported as well (under the 'extra' key). This makes sense for values that take some
    #     time to process.
    #
    #     If the TINGS_ALL_PROPERTIES_KEY value is contained in the 'additional_values' list, all 'provided' keys will be exported.
    #
    #     Returns:
    #         a dict with a 'required' key containing required values, and an optional 'extra' key containing additional values.
    #     """
    #
    #     meta = {"name": self.name, "namespace": self.namespace}
    #     result: Dict[str, Any] = {"meta": meta}
    #
    #     req: Dict[str, Any] = await self.current_input  # type: ignore
    #     result["required"] = req
    #
    #     if additional_values:
    #         if TINGS_ALL_PROPERTIES_KEY in additional_values:
    #             extra_value_names: Iterable[str] = self.output_map.keys()
    #         else:
    #             _extra_value_names: List[str] = []
    #             for val in additional_values:
    #                 if val not in _extra_value_names:
    #                     _extra_value_names.append(val)
    #             extra_value_names = _extra_value_names
    #
    #         extra = await self.get_values(*extra_value_names)
    #         result["extra"] = extra
    #
    #     return result

    @property
    def current_state(self) -> Dict[str, Any]:

        orig_vals = self._calculate_current()

        return self.meta.translate_provides_to_output(orig_vals)

    @property
    def is_current_state_valid(self) -> bool:

        if self._value_cache is None:
            return False
        orig_vals = self._calculate_current()

        for v in orig_vals.values():
            if (
                v == NO_VALUE_MARKER
                or isinstance(v, TingValueError)
                or issubclass(v.__class__, TingValueError)
            ):
                return False

        return True

    def input_changed(self) -> None:

        changed = {}

        old_values = copy.copy(self._calculate_current())
        self._value_cache = None

        # call hook that a inheriting class can override
        self._input_changed()

        if self._always_retrieve_values:
            wrap_async_task(self.get_values, resolve=True)

        cv = self._calculate_current()
        for k in self.provides().keys():

            old = old_values.get(k, NO_VALUE_MARKER)
            current = cv.get(k, NO_VALUE_MARKER)

            if current == old:
                continue

            changed[k] = {"old": old, "new": current}

        self.publish_event(TingsEvent.TING_INPUT_CHANGED)

        if changed:
            self.publish_event(
                TingsEvent.TING_VALUES_CHANGED, changed=changed, current=cv
            )

        to_notify: Dict[Ting, List[str]] = {}

        for key, ti in self._upstream.items():
            for ting in ti:
                to_notify.setdefault(ting, []).append(key)

        for ting in to_notify.keys():
            ting.input_changed()

    def _input_changed(self) -> None:
        pass

    def _add_upstream(self, child: "Ting", *value_names: str) -> None:
        """Register this Ting with one or several value name to an upstream Ting."""

        for vn in value_names:
            self._upstream.setdefault(vn, []).append(child)

    def __eq__(self, other):

        if not other or not issubclass(other.__class__, Ting):
            return False

        return self.full_name == other.full_name

    def __hash__(self):

        return hash(self.full_name)

    def __lt__(self, other):
        if not other or not issubclass(other.__class__, Ting):
            return False
        return self.name < other.name

    def info(self) -> Dict[str, Any]:

        result: Dict[str, Any] = {}
        result["provides"] = self.provides()
        result["requires"] = self.requires()
        result["input"] = self.meta.input_args
        result["output"] = self.meta.output_args
        result["value_cache"] = self._calculate_current()

        return result

    def __repr__(self):
        if hasattr(self, "full_name"):
            return f"{type(self).__name__}(full_name='{self.full_name}' provides={list(self.provides().keys())} requires={list(self.requires().keys())})"
        else:
            return f"{type(self).__name__}(--not initialized--)"

    @property
    def current_input(self) -> Mapping[str, Any]:

        return self._input_ting.get_current_input()

    def set_input(self, _ignore_errors: bool = False, **input_values: Any) -> None:

        input_args = self.meta.input_args

        errors = {}
        for k, v in input_values.items():
            child_arg = input_args.get_child_arg(k)
            if child_arg is None:
                if not _ignore_errors:
                    ae = ArgValidationError(
                        input_args, input_values, msg=f"Argument '{k}' not allowed."
                    )
                    errors[k] = ae
                continue

            if not _ignore_errors:
                val = child_arg.validate(v, raise_exception=False)
                if isinstance_or_subclass(val, ArgValidationError):
                    errors[k] = val

        if errors:
            raise ArgValidationError(input_args, input_values, child_errors=errors)

        self._input_ting.set_values(**input_values)


class SimpleTing(Ting):
    """Default (abstract) implementation of a Ting.

    Use this as a base class when implementing your own Ting class.
    """

    def __init__(self, name: str, meta: TingMeta) -> None:

        super().__init__(name=name, meta=meta)

    def _calculate_value_state(
        self, current_state: Mapping[str, Any], *value_names: str
    ) -> Tuple:

        if not value_names:
            _value_names: Iterable[str] = self.provides().keys()
        else:
            _value_names = value_names

        if self._value_cache is None:
            raise Exception("Value cache not populated, this is most likley a bug.")

        cached = {}
        left = []
        for tn in _value_names:
            if (
                tn in current_state.keys()
                and current_state[tn] != NO_VALUE_MARKER
                and not isinstance(self._value_cache[tn], TingValueError)
                and not issubclass(self._value_cache[tn].__class__, TingValueError)
            ):
                cached[tn] = current_state[tn]
            else:
                left.append(tn)

        return (left, cached)

    def _pre_assemble_hook(self) -> None:
        """Utility hook method, can be overwritten by implementing classes."""

        pass

    def get_requirements(self) -> Tuple[Mapping[str, Any], Iterable["TingTask"]]:

        requirement_values: Dict[str, Any] = {}
        reqs: List[TingTask] = []

        for r_ting, req_value_names in self.requirement_map.items():
            translated = self.meta.translate_required_names_to_input(*req_value_names)
            req_ting_task = r_ting._assemble_retrieval_tasks(*translated)

            # TODO: check if all necessary requirement keys are ready, not the whole thing?
            if req_ting_task.ready:
                temp = {}
                for k, v in req_ting_task.result.items():
                    if k in translated:
                        temp[k] = v

                translated_result = self.meta.translate_input_to_required(temp)
                requirement_values.update(translated_result)
            else:
                reqs.append(req_ting_task)

        return (requirement_values, reqs)

    def _assemble_retrieval_tasks(self, *value_names: str) -> "TingTask":

        values_old: Dict[str, Any] = self._calculate_current()

        self._pre_assemble_hook()
        tt = TingTask(ting=self, old_values=values_old)

        unretrieved_value_names, cached = self._calculate_value_state(
            values_old, *value_names
        )

        if unretrieved_value_names:
            # means we need to do some computation...

            tt.set_cached_values(cached)

            tt.set_task(func=self.retrieve, value_names=unretrieved_value_names)

            req_values, reqs = self.get_requirements()

            if req_values:
                tt.add_requirement_value(req_values)
            for r in reqs:
                tt.add_requirement(r)
            #
            # for r_ting, req_value_names in self.requirement_map.items():
            #     translated = self.meta.translate_required_names_to_input(
            #         *req_value_names
            #     )
            #     req_ting_task = r_ting._assemble_retrieval_tasks(*translated)
            #
            #     # TODO: check if all necessary requirement keys are ready, not the whole thing?
            #     if req_ting_task.ready:
            #         temp = {}
            #         for k, v in req_ting_task.result.items():
            #             if k in translated:
            #                 temp[k] = v
            #
            #         translated_result = self.meta.translate_input_to_required(temp)
            #         tt.add_requirement_value(translated_result)
            #     else:
            #         tt.add_requirement(req_ting_task)

        else:
            tt.set_result(result=cached)

        return tt

    @abstractmethod
    async def retrieve(self, *value_names: str, **requirements) -> Mapping[str, Any]:

        pass


class TingTask(object):
    def __init__(self, ting: "Ting", old_values: Mapping[str, Any] = None):

        self._ting = ting
        if old_values is None:
            old_values = ting._calculate_current()
        self._old_values: Mapping[str, Any] = old_values

        self._cached_values: Optional[MutableMapping[str, Any]] = None
        self._result: Optional[Mapping[str, Any]] = None

        self._func: Optional[Callable] = None
        self._value_names: Optional[Iterable[str]] = None

        self._requirements: List[TingTask] = []

        self._requirement_values: Dict[str, Any] = {}

        self._ready: bool = False
        self._success: bool = False

    @property
    def meta(self) -> TingMeta:
        return self._ting.meta

    # @meta.setter
    # def meta(self, meta: TingMeta):
    #
    #     if meta is None:
    #         meta = {}
    #     self._meta = meta

    @property
    def ready(self) -> bool:
        return self._ready

    @property
    def success(self) -> bool:
        return self._success

    @property
    def func(self) -> Optional[Callable]:
        return self._func

    @property
    def value_names(self) -> Optional[Iterable[str]]:
        return self._value_names

    def set_task(self, func: Callable, value_names: List[str]):

        if self._func or self._value_names:
            raise Exception("func or value_names attribute already set")

        self._func = func
        self._value_names = value_names

    def add_requirement(self, req: "TingTask"):

        self._requirements.append(req)

    def add_requirement_value(self, req_value: Mapping[str, Any]):

        self._requirement_values.update(req_value)

    def set_cached_values(self, cached_values: Dict[str, Any]):

        self._cached_values = cached_values

    async def run_ting_task(self, new_requirement_values) -> Dict[str, Any]:

        if (
            self._cached_values is None
            or self._value_names is None
            or self._func is None
        ):
            raise Exception(
                "Cached values or value names or function not populated yet, this is most likely a bug."
            )

        try:

            requirements = {}
            for k, v in self._cached_values.items():
                if k in self._value_names:
                    continue

            requirements.update(self._requirement_values)
            requirements.update(new_requirement_values)

            requirements_final = {}

            for k, v in requirements.items():

                # optional = self._ting.requires()[k].endswith("?")
                # if v == NO_VALUE_MARKER and not optional:
                #     raise TingTaskException(
                #         msg="Can't run task.",
                #         reason=f"Requirement value '{k}' is not resolved (yet).",
                #         ting_task=self,
                #     )
                if isinstance(v, Exception) or issubclass(v.__class__, Exception):
                    raise TingTaskException(
                        msg=f"Error in requirement value '{k}': {v}",
                        ting_task=self,
                        parent=v,
                    )

                if v == NO_VALUE_MARKER:
                    continue

                requirements_final[k] = v

            # validating requirements/adding defaults
            args = self._ting.meta.requires_args
            requirements_validated = args.validate(
                requirements_final, raise_exception=True
            )

            run_result = await self._func(*self._value_names, **requirements_validated)

            if run_result is None:
                log.debug(
                    "Error running ting task: 'None' value returned, this is probably a bug"
                )
                run_result = {}
                for vn in self._value_names:
                    run_result[vn] = TingValueError(
                        value_name="vn",
                        ting=self._ting,
                        msg=f"Can't extract values '{', '.join(self._value_names)}' from 'None' result (ting: '{self._ting.full_name}'). This is probably a bug.",
                        reason="'None' return value not allowed.",
                    )

            else:
                self._success = True
        except Exception as e:
            log.debug(f"Error running ting task: {e}", exc_info=True)
            run_result = {}
            for vn in self._value_names:
                if isinstance_or_subclass(e, FrklException):
                    run_result[vn] = TingValueError(
                        ting=self._ting, value_name=vn, parent=e
                    )
                else:

                    run_result[vn] = TingValueError(
                        parent=e, ting=self._ting, value_name=vn
                    )

        self._ready = True

        result = self._ting._set_result(run_result)

        keys = list(self._value_names)
        keys.extend(self._cached_values.keys())
        translated_result = {}
        for tvn in keys:
            value = result[tvn]
            if isinstance(value, Exception) or issubclass(value.__class__, Exception):
                self._success = False
            translated_key = self._ting.meta.get_output_key(tvn)
            translated_result[translated_key] = value

        return translated_result

    async def run_in_place(self, raise_exception=False) -> Dict[str, Any]:

        new_requirement_values: Dict[str, Any] = {}

        async def run_requirement(req):
            result = await req.run_in_place(raise_exception=raise_exception)
            new_requirement_values.update(result)

        async with create_task_group() as tg:

            for req in self._requirements:
                await tg.spawn(run_requirement, req)

        result = await self.run_ting_task(new_requirement_values=new_requirement_values)

        if raise_exception:
            exceptions: Dict[str, Exception] = {}
            for k, v in result.items():
                if isinstance(v, Exception) or issubclass(v.__class__, Exception):
                    exceptions[k] = v  # type: ignore

            if exceptions:
                if len(exceptions) == 1:
                    k = next(iter(exceptions))
                    e = exceptions[k]
                    raise TingTaskException(ting_task=self, parent=e)

                else:
                    parent = FrklMultiParentException(*exceptions.values())
                    raise TingTaskException(
                        ting_task=self,
                        msg=f"Error retrieving values: {', '.join(exceptions.keys())}.",
                        parent=parent,
                    )

        return result

    @property
    def result(self) -> Mapping[str, Any]:

        if not self._ready:
            raise Exception("Result not ready yet.")
        if self._result is None:
            raise Exception("Result value set to None.")
        return self._result

    def set_result(self, result: Mapping[str, Any]):
        self._ready = True
        self._result = result

    def __repr__(self):

        success = "" if not self.ready else f" success={self.success}"
        return f"[{self.__class__.__name__}: ting={self._ting.full_name}, ready={self.ready}{success}]"


class InputTing(Ting):
    def __init__(
        self,
        name: str,
        meta: TingMeta,
        provides: Optional[Mapping[str, Union[str, Mapping[str, Any]]]] = None,
    ):

        if provides is None:
            provides = {}
        self._provides: Mapping[str, Union[str, Mapping[str, Any]]] = provides

        # self._args_obj: Optional[RecordArg] = None
        self._tingistry_obj = meta.tingistry

        super().__init__(name=name, meta=meta)

    # @property
    # def _args(self) -> Optional[RecordArg]:
    #
    #     if self._args_obj is not None:
    #         return self._args_obj
    #
    #     self._args_obj = self._tingistry_obj.arg_hive.create_record_arg(
    #         childs=self.provides(), id=f"{self.full_name}"
    #     )
    #     return self._args_obj

    def set_provides(self, provides: Mapping[str, Union[str, Mapping[str, Any]]]):

        self._upstream.clear()
        self._provides = provides
        self._args_obj = None

    def set_requirements(self, *requirements: "Ting"):

        raise Exception("Setting requirements not supported for type InputTing")

    def provides(self) -> Mapping[str, Union[str, Mapping[str, Any]]]:

        return self._provides

    def requires(self) -> Mapping[str, Union[str, Mapping[str, Any]]]:
        return {}

    @abstractmethod
    def set_values(self, **input_values: Any):
        pass

    @abstractmethod
    def get_current_input(self) -> Mapping[str, Any]:
        pass

    async def get_values(
        self, *value_names: str, resolve=True, raise_exception: bool = False
    ) -> Mapping[str, Any]:

        if not value_names:
            _value_names: Iterable = self.provides().keys()
        else:
            _value_names = value_names

        result = {}
        input_vals = self.get_current_input()
        for k in _value_names:
            result[k] = input_vals.get(k, NO_VALUE_MARKER)

        return result

    def _assemble_retrieval_tasks(self, *value_names: str) -> "TingTask":

        vals = self.get_current_input()
        tt = TingTask(ting=self, old_values=vals)
        tt.set_result(vals)

        return tt


class DictInputTing(InputTing):
    def __init__(
        self,
        name: str,
        meta: TingMeta,
        provides: Optional[Mapping[str, Union[str, Mapping[str, Any]]]] = None,
    ):

        self._input_values: Dict[str, Any] = {}

        super().__init__(name=name, provides=provides, meta=meta)

    def get_current_input(self) -> Mapping[str, Any]:

        return self._input_values

    def set_values(self, **input_values: Any):

        to_set = {}
        # errors = {}
        for k, v in input_values.items():
            if k not in self.provides().keys():
                continue

            to_set[k] = v

        self._input_values.update(to_set)
        self.input_changed()

    def _input_changed(self):

        self._set_value_cache(self._input_values)
