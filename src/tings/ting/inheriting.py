# -*- coding: utf-8 -*-
import logging
from typing import TYPE_CHECKING, Any, Dict, Iterable, Optional, Union

from tings.events import TingsEvent
from tings.ting import Ting, TingMeta


if TYPE_CHECKING:
    from tings.tingistry import Tingistry

log = logging.getLogger("tings")


class InheriTing(Ting):
    def __init__(self, name: str, *args, meta: TingMeta, **kwargs):

        self._subscriptions: Dict[str, Dict[str, Any]] = {}

        if meta is None:
            raise Exception(
                f"No meta values provided when trying to create InheriTing '{name}', can't access tingistry."
            )
        self._tingistry_obj: Tingistry = meta.tingistry

        super().__init__(name=name, meta=meta)

    async def _get_values_from_ting(
        self, ting: Union[Ting, str], *value_names: str
    ) -> Dict[str, Any]:

        if isinstance(ting, str):
            _ting: Ting = self._tingistry_obj.get_ting(  # type: ignore
                ting, raise_exception=True
            )
        else:
            _ting = ting

        topic = _ting.subscription_topic
        subscribed = topic in self._subscriptions.keys()

        cached = {}
        missing_values: Optional[Iterable]

        if not value_names:
            missing_values = _ting.provides().keys()
        elif self._subscriptions.get(topic, None):
            missing_values = []
            cached_values = self._subscriptions.setdefault(topic, {})
            for val_name in value_names:
                if val_name in cached_values.keys():
                    cached[val_name] = cached_values[val_name]
                else:
                    missing_values.append(val_name)

        else:
            missing_values = value_names

        if not missing_values:
            return cached

        values = await _ting.get_values(*missing_values, resolve=True)

        for k, v in values.items():  # type: ignore
            self._subscriptions.setdefault(topic, {})[k] = v
            cached[k] = v

        if not subscribed:
            _ting.subscribe_to(
                self._subscription_updated, TingsEvent.TING_VALUES_CHANGED
            )

        return cached

    def _subscription_updated(self, **kwargs):

        print("UPDATED")
        print(kwargs["source"])
        print(kwargs["event_name"])
        print(list(kwargs.keys()))
