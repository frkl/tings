# -*- coding: utf-8 -*-
import os
import sys

from appdirs import AppDirs


tings_app_dirs = AppDirs("tings", "frkl")

if not hasattr(sys, "frozen"):
    TINGS_MODULE_BASE_FOLDER = os.path.dirname(__file__)
    """Marker to indicate the base folder for the `tings` module."""
else:
    TINGS_MODULE_BASE_FOLDER = os.path.join(sys._MEIPASS, "tings")  # type: ignore
    """Marker to indicate the base folder for the `tings` module."""

TINGS_RESOURCES_FOLDER = os.path.join(TINGS_MODULE_BASE_FOLDER, "resources")

FROZEN_TING_SPEC_KEY = "ting_spec"
"""The key that denotes the spec in a frozen tings archive."""
FROZEN_TING_LIST_KEY = "ting_list"
"""The key that denotes the list of frozen ting items in a frozen tings archive."""
FROZEN_TING_METADATA_KEY = "metadata"
"""The key that denotes the metadata contained in a frozen tings archive."""

TING_PROPERTY_CACHE_MISS_KEY = "__ting_cache_miss__"
"""Marker to indicate a value is not in the cache."""


# ======================================
# TingValue metadata defaults

NO_CACHE_KEY = "no_cache"
"""Key name for a value that states whether a property should be cached or not."""

DEFAULT_INPUT_TING_NAME = "ting_input"
"""Reserved key for the dynamically generated input SpecTingAssembly within a SpecTingsTing."""
NO_VALUE_MARKER = "__NO_VALUE__"


TINGS_DEFAULT_MODULES = ["tings.ting.*", "tings.ting.common.*", "tings.makers.*"]
"""Default modules to pre-load. Used to find all subclasses of the 'Ting' class."""

DEFAULT_TING_CLASS_NAMESPACE = "ting_classes"
"""Default namespace for ting classes (if not provided)."""

DEFAULT_TING_TYPE_NAMESPACE = "prototings"
"""Default namespace for ting types (if not provided)."""

DEFAULT_TING_NAMESPACE = "tings"
"""Default namespace for tings (if not provided)."""

TINGS_SEED_ID_KEY = "__seed_id__"
"""Key for the seed id"""

TINGS_ALL_PROPERTIES_KEY = "__all__"
"""Key to signify all properties should be exported/used."""

TINGS_ITEM_ABSENT_KEY = "__item_absent__"
"""Key to signify the source for a ting id (with the context of a TingMaker) does not exist anymore."""

PROTOTING_META_SCHEMA = {
    "tingistry": "tingistry",
    "defaults": {"name": "string", "namespace": "string", "labels": "dict"},
    "input": "dict",
    "output": "dict",
    "is_singleting": "boolean",
    "ting_init": "dict",
}

TING_META_SCHEMA = {
    # "tingistry": "tingistry",
    "publish": {"disabled": "boolean?"},
    "info": {"prototing": "string", "tingistry": "name", "hidden": "boolean?"},
    "labels": "dict",
}
