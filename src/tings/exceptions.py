# -*- coding: utf-8 -*-
import logging
from typing import TYPE_CHECKING, Any, Optional

from frkl.common.exceptions import FrklException, FrklExtendedException


log = logging.getLogger("tings")

if TYPE_CHECKING:
    from tings.ting import Ting, TingTask


class TingException(FrklExtendedException):
    def __init__(
        self, ting: "Ting" = None, parent: Optional[Exception] = None, **kwargs
    ):

        self._ting = ting

        super().__init__(parent=parent, **kwargs)


class TingConfigException(TingException):
    def __init__(self, *args, **kwargs):

        super().__init__(*args, **kwargs)


class TingRegistryException(FrklException):
    def __init__(self, *args, **kwargs):

        super().__init__(*args, **kwargs)


class TingTaskException(FrklExtendedException):
    def __init__(self, ting_task: "TingTask", **kwargs):

        self._ting_task: TingTask = ting_task

        super().__init__(**kwargs)


class TingValueError(FrklExtendedException):
    def __init__(self, value_name: str, ting: "Ting", **kwargs: Any):

        self._value_names: str = value_name
        self._ting: "Ting" = ting

        super().__init__(**kwargs)

    def __repr__(self):
        return self.message_short

    def __str__(self):
        return self.message_short
