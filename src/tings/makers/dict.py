# -*- coding: utf-8 -*-
from abc import abstractmethod
from typing import (
    Any,
    AsyncIterator,
    Hashable,
    Mapping,
    MutableMapping,
    Optional,
    Union,
)

from frkl.args.arg import RecordArg
from frkl.common.formats import CONTENT_TYPE
from frkl.common.formats.auto import AutoInput
from tings.makers import TingMaker
from tings.ting import TingMeta


class DictTingMaker(TingMaker):
    def __init__(
        self,
        name: str,
        meta: TingMeta,
        prototing: str,
        ting_target_namespace: str = None,
    ):

        super().__init__(
            name=name,
            prototing=prototing,
            ting_target_namespace=ting_target_namespace,
            meta=meta,
        )

    @abstractmethod
    def get_dict(self) -> Mapping[Hashable, MutableMapping[str, Any]]:
        pass

    async def get_item(
        self, id: Hashable, required_values: RecordArg
    ) -> Union[str, MutableMapping[str, Any], None]:

        return self.get_dict()[id]

    async def find_all_ids(self) -> AsyncIterator[Hashable]:

        for k in self.get_dict().keys():
            yield k

        return


class SmartInputDictTingMaker(DictTingMaker):
    def __init__(
        self,
        name: str,
        meta: TingMeta,
        prototing: str,
        ting_target_namespace: str = None,
        content_type: CONTENT_TYPE = CONTENT_TYPE.auto,
    ):

        if content_type is None:
            content_type = CONTENT_TYPE.auto

        self._url: Optional[str] = None
        self._content_type: CONTENT_TYPE = content_type
        super().__init__(
            name=name,
            prototing=prototing,
            ting_target_namespace=ting_target_namespace,
            meta=meta,
        )

    def get_dict(self) -> Mapping[Hashable, MutableMapping[str, Any]]:

        if self._url is None:
            return {}

        si = AutoInput(input_value=self._url, content_type=self._content_type)
        content: Mapping[Hashable, MutableMapping[str, Any]] = si.get_content()  # type: ignore

        return content

    def set_url(self, url: str):
        self._url = url

        self.invalidate()
