# -*- coding: utf-8 -*-
import collections
import logging
from typing import Any, Dict, List, Mapping, MutableMapping, Union

from frkl.common.strings import find_free_name
from tings.ting import SimpleTing, Ting, TingMeta
from tings.utils import process_ting_name


log = logging.getLogger("tings")


class TingTing(SimpleTing):
    def __init__(
        self, name: str, meta: TingMeta, prototings: List[Union[str, Dict]]
    ) -> None:

        _name, _namespace, _full_name = process_ting_name(name)

        tingistry_obj = meta.tingistry

        self._prototings = prototings
        self._tings: Dict[str, Ting] = {}

        self._provides_details: MutableMapping[str, Mapping] = {}
        self._provides_map: MutableMapping[str, str] = {}
        self._requires_details: MutableMapping[str, Mapping] = {}
        self._requires_map: MutableMapping[str, str] = {}

        for prototing in prototings:
            if isinstance(prototing, str):
                prototing_name = prototing
                ting_name = find_free_name(
                    stem=prototing_name.split(".")[-1],
                    current_names=self._tings.keys(),
                    sep="_",
                )

            elif isinstance(prototing, collections.abc.Mapping):
                prototing_name = prototing["type_name"]
                ting_name = prototing.get(
                    "name",
                    find_free_name(
                        stem=prototing_name.split(".")[-1],
                        current_names=self._tings.keys(),
                        sep="_",
                    ),
                )
            else:
                raise TypeError(
                    f"Can't create tingting '{_full_name}'.",
                    f"Invalid prototing type '{type(prototing)}: {prototing}",
                )

            ting = tingistry_obj.create_ting(
                prototing=prototing_name, ting_name=ting_name
            )
            ting._item = _full_name
            self._tings[ting_name] = ting

            for k, v in ting.provides().items():
                if k in self._provides_details.keys():
                    raise Exception(f"Duplicate provides key: {k}")
                self._provides_details[k] = {"ting": ting, "type": v}
                self._provides_map[k] = v

            for k, v in ting._requires().items():
                if k in self._requires_details.keys():
                    raise Exception(f"Duplicate requires key: {k}")
                self._requires_details[k] = {"ting": ting, "type": v}

        for ting in self._tings.values():
            reqs = []

            for k in ting.requires().keys():
                if k in self._provides_details.keys():
                    req = self._provides_details[k]["ting"]
                    reqs.append(req)
                else:
                    self._requires_map[k] = ting.requires()[k]
            ting.set_requirements(*reqs)

        super().__init__(name=name, meta=meta)

    def requires(self) -> Mapping[str, Union[str, Mapping[str, Any]]]:

        return self._requires_map

    def provides(self) -> Mapping[str, Union[str, Mapping[str, Any]]]:

        return self._provides_map

    async def retrieve(self, *value_names: str, **requirements) -> Dict[str, Any]:

        input_vals = self.current_input

        reqs: Dict[Ting, Dict] = {}
        for k, v in input_vals.items():  # type: ignore
            reqs.setdefault(self._requires_details[k]["ting"], {})[k] = v

        for ting, vals in reqs.items():
            ting.set_input(**vals)

        result = {}
        for vn in value_names:
            ting = self._provides_details[vn]["ting"]
            val = await ting.get_values(vn, resolve=True)
            result[vn] = val[vn]  # type: ignore

        return result
