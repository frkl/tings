# -*- coding: utf-8 -*-
import logging
import os
from typing import Any, Dict

import anyio
from tings.ting import SimpleTing, TingMeta


log = logging.getLogger("tings")


class TextFile(SimpleTing):
    def __init__(
        self,
        name,
        meta: TingMeta,
        must_exists: bool = False,
        dir_ok: bool = True,
        file_ok: bool = True,
    ):

        self._must_exist = must_exists
        self._file_ok = file_ok
        self._dir_ok = dir_ok

        super().__init__(name=name, meta=meta)

    def provides(self) -> Dict[str, str]:

        return {
            "size": "long",
            "content": "string",
            "base_name": "string",
            "dir_name": "string",
            "base_name_no_ext": "string",
        }

    def requires(self) -> Dict[str, str]:

        return {"path": "string"}

    async def retrieve(self, *value_names: str, **requirements) -> Dict[str, Any]:

        path = requirements["path"]

        result = {}

        if "size" in value_names:
            result["size"] = os.path.getsize(path)

        if "content" in value_names:

            async with await anyio.open_file(path) as f:
                string_content = await f.read()
            result["content"] = string_content

        if "base_name" in value_names:

            result["base_name"] = os.path.basename(path)

        if "dir_name" in value_names:

            result["dir_name"] = os.path.dirname(path)

        if "base_name_no_ext" in value_names:
            result["base_name_no_ext"] = os.path.splitext(os.path.basename(path))[0]

        return result
