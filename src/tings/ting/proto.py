# -*- coding: utf-8 -*-
import collections
import copy
import logging
from abc import ABCMeta, abstractmethod
from typing import (
    TYPE_CHECKING,
    Any,
    Dict,
    Mapping,
    MutableMapping,
    Optional,
    Type,
    Union,
)

from frkl.args.arg import RecordArg
from frkl.common.exceptions import FrklException
from tings.events import TingsEvent
from tings.exceptions import TingConfigException, TingRegistryException
from tings.ting import SimpleTing, Ting, TingMeta
from tings.ting.tings import Tings


if TYPE_CHECKING:
    from tings.tingistry import Tingistry

log = logging.getLogger("tings")


def prepare_prototing_meta(
    prototing_name: str,
    prototing_namespace: Optional[str],
    ting_meta: Optional[Mapping[str, Any]],
    ting_input: Optional[Mapping[str, str]],
    ting_output: Optional[Mapping[str, str]],
    ting_init: Mapping[str, Any],
    tingistry: "Tingistry",
) -> MutableMapping[str, Any]:
    """Utility method to help prepare a TingMeta object."""

    if ting_meta is None:
        _meta = {}
    else:
        _meta = dict(copy.deepcopy(ting_meta))

    if "defaults" in _meta.keys() and not isinstance(
        _meta["defaults"], collections.Mapping
    ):
        raise TypeError("'defaults' key in ting metadata must be a mapping")

    if prototing_namespace is None:
        full_name = prototing_name
    else:
        full_name = f"{prototing_namespace}.{prototing_name}"

    m_defaults: MutableMapping = _meta.setdefault("defaults", {})
    # m_defaults.setdefault("labels", {})
    if "name" not in m_defaults.keys():
        m_defaults["name"] = prototing_name
    if "namespace" not in m_defaults.keys():
        m_defaults["namespace"] = prototing_namespace

    if ting_input:
        if "input" in _meta.keys():
            raise TingRegistryException(
                msg=f"Can't register type '{prototing_name}'.",
                reason=f"'input' value specified twice: {ting_input} - {_meta['input']}",
                solution="Either use the 'input' argument in 'register_ting_type', or the 'input' key for the 'meta' argument.",
            )
        _meta.update({"input": ting_input})

    if ting_output:
        if "output" in _meta.keys():
            raise TingRegistryException(
                msg=f"Can't register type '{prototing_name}'.",
                reason=f"'output' value specified twice: {ting_output} - {_meta['output']}",
                solution="Either use the 'output' argument in 'register_ting_type', or the 'output' key for the 'meta' argument.",
            )
        _meta.update({"output": ting_output})

    _meta["tingistry"] = tingistry
    _meta["ting_init"] = ting_init
    _meta["prototing_name"] = full_name

    return _meta


def create_ting_obj(
    meta: TingMeta,
    class_details: Mapping[str, Any],
    ting_init: Mapping[str, Any],
    ting_name: Optional[str] = None,
) -> Ting:

    if not ting_name:

        namespace = meta.defaults["namespace"]
        if not namespace:
            namespace = meta.prototing.full_name

        ting_name = meta.tingistry.find_free_ting_name(
            namespace=namespace, name=meta.defaults["name"]
        )

    type_cls = class_details["class"]
    try:
        obj = type_cls(name=ting_name, meta=meta, **ting_init)
    except (TypeError) as e:
        raise TingConfigException(
            msg=f"Can't create Ting '{ting_name}' from ProtoTing '{meta.prototing.full_name}'",
            reason=f"Invalid or missing initialization arguments: {e}",
        )
    log.debug(f"created ting (proto_ting: {meta.prototing.full_name}): {obj.full_name}")

    return obj


class ProtoTing(Ting):
    def __init__(
        self, name: str, meta: TingMeta, ting_class: Union[str, Type], **ting_init: Any
    ):

        self._tingistry_obj: Tingistry = meta.tingistry

        self._ting_init = ting_init

        self._ting_class_details: Mapping[
            str, Any
        ] = self._tingistry_obj.get_ting_class_details(ting_class)

        self._ting_input_args: Optional[RecordArg] = None

        # super().__init__(self, name=name, meta=meta)
        # ref_ting = self.get_reference_ting()
        # args = ref_ting.get_arg_types()
        #
        # if args:
        #     for arg_name, arg_details in args.items():
        #         self._tingistry_obj.arg_hive.register_derived_arg_type(
        #             id=arg_name, **arg_details
        #         )

    @abstractmethod
    def get_reference_ting(self) -> Ting:
        pass

    @property
    def ting_requires(self) -> Mapping[str, Union[str, Mapping[str, Any]]]:
        return self.get_reference_ting().requires()

    @property
    def ting_provides(self) -> Mapping[str, Union[str, Mapping[str, Any]]]:
        return self.get_reference_ting().provides()

    @property
    def ting_input(self) -> Mapping[str, Union[str, Mapping[str, Any]]]:
        return self.get_reference_ting()._input_ting.provides()

    @property
    def ting_input_args(self) -> RecordArg:

        if self._ting_input_args is None:
            self._ting_input_args = self.tingistry.arg_hive.create_record_arg(
                self.ting_input
            )

        return self._ting_input_args

    def create_ting_obj(self, ting_name: Optional[str], publish: bool = True) -> Ting:
        ting = self._create_ting_obj(ting_name=ting_name)

        if publish:
            self.publish_event(TingsEvent.TING_CREATED, ting=ting)

        return ting

    @abstractmethod
    def _create_ting_obj(self, ting_name: Optional[str]) -> Ting:
        pass


class SingleTing(SimpleTing, ProtoTing):
    def __init__(
        self,
        tingistry: "Tingistry",
        prototing_name: str,
        prototing_namespace: Optional[str],
        ting_name: str,
        ting_class: Union[str, Type],
        ting_meta: Optional[Mapping[str, Any]] = None,
        ting_input: Optional[Mapping[str, str]] = None,
        ting_output: Optional[Mapping[str, str]] = None,
        **ting_init: Any,
    ) -> None:

        if "meta" in ting_init.keys():
            raise ValueError(
                "ProtoTing is a special case, can't be created with a 'meta' parameter."
            )

        _ting_meta = prepare_prototing_meta(
            prototing_name,
            prototing_namespace,
            ting_input=ting_input,
            ting_output=ting_output,
            ting_meta=ting_meta,
            ting_init=ting_init,
            tingistry=tingistry,
        )

        _ting_meta["is_singleting"] = True

        self._reference_ting: Optional[Ting] = None

        self._ting_name = ting_name
        self._child: Optional[Ting] = None

        full_name = _ting_meta["prototing_name"]

        meta = TingMeta(**_ting_meta)

        ProtoTing.__init__(
            self, name=full_name, meta=meta, ting_class=ting_class, **ting_init
        )
        Ting.__init__(self, name=full_name, meta=meta)

    def _create_ting_obj(self, ting_name: Optional[str] = None) -> Ting:

        if self._ting_name and self._ting_name != ting_name:
            raise FrklException(
                msg=f"Can't create singleting child ting for '{self.full_name}'.",
                reason=f"Requested ting different than the internally stored one: {self._ting_name} - {ting_name}",
            )

        return self.ting

    def get_reference_ting(self) -> Ting:

        return self.ting

    @property
    def ting(self) -> Ting:

        if self._child is not None:
            return self._child

        self._child = create_ting_obj(
            meta=self._meta,
            class_details=self._ting_class_details,
            ting_init=self._ting_init,
            ting_name=self._ting_name,
        )
        return self._child

    async def retrieve(self, *value_names: str, **requirements) -> Mapping[str, Any]:
        return {}

    def provides(self) -> Dict[str, str]:
        return {}


class ProtoTings(ProtoTing, Tings):
    def __init__(
        self,
        tingistry: "Tingistry",
        prototing_name: str,
        prototing_namespace: Optional[str],
        ting_class: Union[str, Type],
        ting_meta: Optional[Mapping[str, Any]],
        ting_input: Optional[Mapping[str, str]] = None,
        ting_output: Optional[Mapping[str, str]] = None,
        **ting_init: Any,
    ):
        if "meta" in ting_init.keys():
            raise ValueError(
                "ProtoTing is a special case, can't be created with a 'meta' parameter."
            )

        _ting_meta = prepare_prototing_meta(
            prototing_name,
            prototing_namespace,
            ting_meta=ting_meta,
            ting_input=ting_input,
            ting_output=ting_output,
            ting_init=ting_init,
            tingistry=tingistry,
        )

        self._reference_ting: Optional[Ting] = None

        full_name = _ting_meta["prototing_name"]
        meta = TingMeta(**_ting_meta)

        ProtoTing.__init__(
            self, name=full_name, meta=meta, ting_class=ting_class, **ting_init
        )
        Tings.__init__(self, name=full_name, meta=meta, prototing=full_name)
        # self._tings: Tings = Tings(name=f"{full_name}.ting_objs", prototing=full_name, meta=meta)

    def _create_ting_obj(self, ting_name: Optional[str] = None) -> Ting:

        ting = create_ting_obj(
            meta=self._meta,
            class_details=self._ting_class_details,
            ting_init=self._ting_init,
            ting_name=ting_name,
        )

        self.add_child(ting)

        return ting

    def get_reference_ting(self) -> Ting:

        if self._reference_ting is not None:
            return self._reference_ting

        # self._reference_ting = self.create_ting_obj(
        #     ting_name=f"{self.full_name}.reference_obj",
        #     meta={"publish": {"disabled": True}, "tingistry": {"register": False}},
        # )

        self._reference_ting = self.create_ting_obj(
            ting_name=f"{self.full_name}.reference_obj", publish=False
        )

        return self._reference_ting


class ProtoTingBuilder(metaclass=ABCMeta):
    @abstractmethod
    def build_proto_ting(
        self,
        prototing_name: str,
        ting_class: Union[str, Type],
        ting_name: Optional[str] = None,
        ting_meta: Optional[Dict[str, Any]] = None,
        ting_input: Optional[Dict[str, str]] = None,
        ting_output: Optional[Dict[str, str]] = None,
        meta: Optional[Dict[str, Any]] = None,
        **ting_init: Any,
    ) -> ProtoTing:

        pass
