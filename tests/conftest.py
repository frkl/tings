#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
    Dummy conftest.py for tings.

    If you don't know what this is for, just leave it empty.
    Read more about conftest.py under:
    https://pytest.org/latest/plugins.html
"""
import os
import random
import shutil
import string
import tempfile

import pytest
from tings.ting.common.string import *  # noqa
from tings.ting.proto import ProtoTing
from tings.tingistry import Tingistries, Tingistry


def randomString(stringLength=8):
    letters = string.ascii_lowercase
    return "".join(random.choice(letters) for i in range(stringLength))


@pytest.fixture
def tingistry() -> Tingistry:

    Tingistries().remove_all_tingistries()

    return Tingistries().add_tingistry(randomString(8))


@pytest.fixture
def string_proto_ting(tingistry) -> ProtoTing:

    tings_proto_name = tingistry.register_prototing(
        "test.proto.tings", ting_class="string_ting"
    )
    tings_proto_ting = tingistry.get_ting(tings_proto_name)

    return tings_proto_ting


@pytest.fixture
def dict_ting_folder() -> str:

    tempdir = tempfile.mkdtemp()

    target = os.path.join(tempdir, "target")
    source = os.path.join(os.path.dirname(__file__), "resources", "dict_tings")

    shutil.copytree(source, target)

    return target
