# -*- coding: utf-8 -*-
from typing import Any, Dict

from frkl.common.formats.auto import auto_parse_string
from tings.exceptions import TingConfigException
from tings.ting import SimpleTing, TingMeta


class DictTing(SimpleTing):
    def __init__(self, name, meta: TingMeta, key_map: Dict[str, Any] = None):

        if key_map is None:
            key_map = {}
        self._key_map = key_map
        if "dict" in self._key_map.keys():
            raise TingConfigException(
                msg="Can't create Dict.", reason="Key 'dict' not allowed in key_map."
            )

        self._provides = {"dict": "dict"}

        for k, v in self._key_map.items():
            self._provides[k] = "any"

        super().__init__(name=name, meta=meta)

    def provides(self) -> Dict[str, str]:

        return self._provides

    def requires(self) -> Dict[str, str]:

        return {"content": "string"}

    async def retrieve(self, *value_names: str, **requirements) -> Dict[str, Any]:

        parsed_dict = requirements.get("_parsed_dict", None)
        if parsed_dict is None:
            parsed_dict = auto_parse_string(requirements["content"])

        result = {"_parsed_dict": parsed_dict}

        if "dict" in value_names:
            result["dict"] = parsed_dict

        for k, v in self._key_map.items():

            result[k] = parsed_dict.get(k, v)

        return result
