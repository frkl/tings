# -*- coding: utf-8 -*-
import copy
import logging
from abc import abstractmethod
from typing import (
    TYPE_CHECKING,
    Any,
    Dict,
    Iterable,
    List,
    Mapping,
    MutableMapping,
    Optional,
    Union,
)

from frkl.common.exceptions import FrklException
from frkl.common.types import isinstance_or_subclass
from sortedcontainers import SortedDict
from tings.events import TingsEvent
from tings.exceptions import TingException
from tings.ting import Ting, TingMeta, TingTask


if TYPE_CHECKING:
    from tings.ting.proto import ProtoTing

log = logging.getLogger("tings")


class TingBag(Ting):
    """A Ting to hold other Tings, which can be of different types."""

    def __init__(
        self,
        name: str,
        meta: TingMeta,
        child_id: str = None,
        sort_childs: bool = True,
        **kwargs,
    ):

        if child_id is None:
            child_id = "full_name"

        if sort_childs:
            self._childs: Dict[str, Ting] = SortedDict()
        else:
            self._childs = {}

        self._child_id: Optional[str] = child_id

        super().__init__(name=name, meta=meta, **kwargs)

    @property
    def childs(self) -> Mapping[str, Ting]:

        return self._childs

    def _get_child_id(self, child: Ting) -> str:

        if self._child_id == "full_name":
            return child.full_name
        elif self._child_id == "name":
            return child.name
        else:
            raise Exception(
                f"Invalid child id key '{self._child_id}' for things '{self.full_name}', allowed: 'full_name', 'name'"
            )

    def get_child(self, child: Union[str, Ting]):

        if isinstance(child, str):
            _child: str = child
        elif isinstance_or_subclass(child, Ting):
            _child = self._get_child_id(child)  # type: ignore
        else:
            raise TypeError(
                f"Can't determine child for tingbag '{self.full_name}'. Invalid child type '{type(child)}'."
            )

        c = self._childs.get(_child, None)
        if c is None:
            raise TingException(
                ting=self,
                msg=f"Can't retrieve child '{child}'.",
                reason="No child with that name added.",
            )

        return c

    def add_child(self, child: Ting):

        self._childs[self._get_child_id(child)] = child
        self.input_changed()

        self.publish_event(TingsEvent.CHILD_ADDED, child=child)
        self.publish_event(
            TingsEvent.CHILDS_CHANGED, added=[child], updated=[], removed=[]
        )

        return child

    def _input_changed(self):

        self._set_value_cache({"childs": self._childs})

    def remove_child(self, child: Ting):

        child = self._childs.pop(self._get_child_id(child))

        self.input_changed()

        self.publish_event(TingsEvent.CHILD_REMOVED, child=child)
        self.publish_event(
            TingsEvent.CHILDS_CHANGED, added=[], updated=[], removed=[child]
        )

        return child

    def update_child(self, child: Union[str, Ting], invalidate_child=True):

        if not isinstance(child, str):
            child_id = self._get_child_id(child)
        else:
            child_id = child

        _child: Optional[Ting] = self._childs.get(child_id, None)
        if _child is None:
            raise TingException(
                msg=f"Can't update child '{child_id}'.",
                reason="No child with that name exists in this collection.",
            )

        if invalidate_child:
            _child.invalidate()

        self.publish_event(TingsEvent.CHILD_UPDATED, child=_child)
        self.publish_event(
            TingsEvent.CHILDS_CHANGED, added=[], removed=[], updated=[_child]
        )

    @abstractmethod
    def provides(self) -> Mapping[str, Union[str, Mapping[str, Any]]]:
        pass

    @abstractmethod
    def _assemble_retrieval_tasks(self, *value_names: str) -> "TingTask":
        pass


class Tings(TingBag):
    """A Ting to hold other Tings, all of the same type."""

    def __init__(
        self,
        name: str,
        meta: TingMeta,
        prototing: str,
        indexes: List[str] = None,
        child_id: str = None,
        sort_childs: bool = True,
        **kwargs,
    ):

        # namespace = details["namespace"]
        # full_name = details["full_name"]
        # _meta = details["meta"]

        self._child_prototing_name: str = prototing

        if not indexes:
            indexes = []
        elif isinstance(indexes, str):
            indexes = [indexes]

        self._index_keys = indexes
        # for index in self._index_keys:
        #     if index not in self._ting_type_info["provides"].keys():
        #         raise TingException(
        #             ting=self,
        #             msg=f"Can't create tings object '{full_name}'.",
        #             reason=f"Requested index key not among child types' provides keys: {index}",
        #         )

        self._indexes: Mapping[str, str] = {}
        # self._provides = self._get_extra_provides()
        self._provides: MutableMapping[str, str] = {}

        for index in self._index_keys:
            self._provides[f"index_{index}"] = "dict"

        self._provides["childs"] = "dict"

        super().__init__(
            name=name, child_id=child_id, sort_childs=sort_childs, meta=meta, **kwargs
        )

    @property
    def child_prototing(self) -> "ProtoTing":

        return self.meta.tingistry.get_ting(self._child_prototing_name)

    def provides(self) -> Mapping[str, Union[str, Mapping[str, Any]]]:

        return self._provides

    # def _get_extra_provides(self) -> Optional[str, str]:
    #
    #     return None

    def add_child(self, child: Ting):
        result = super().add_child(child)
        return result

    def remove_child(self, child: Ting):

        result = super().remove_child(child)

        return result

    def update_child(self, child: Union[str, Ting], invalidate_child=True):

        result = super().update_child(child, invalidate_child=invalidate_child)

        return result

    def get_index(self, property_name):

        if (
            property_name in self._indexes.keys()
            and self._indexes[property_name] is not None
        ):
            return self._indexes[property_name]

    def _assemble_retrieval_tasks(self, *value_names: str) -> "TingTask":

        if not value_names:
            _value_names: Iterable[str] = self.provides().keys()
        else:
            _value_names = value_names

        values_old = copy.copy(self._calculate_current())

        tt = TingTask(ting=self, old_values=values_old)

        result = {}
        if "childs" in _value_names:
            result["childs"] = self._childs

        for vn in _value_names:
            if not vn.startswith("index_"):
                continue

        tt.set_result(result=result)

        return tt


class SubscripTings(Tings):
    """A Ting to hold other Tings (of the same type), gathered by subscribing to a pub namespace and adding new ones."""

    def __init__(
        self,
        name: str,
        meta: TingMeta,
        prototing: str,
        subscription_namespace: str = None,
        indexes: List[str] = None,
        child_id: str = None,
        sort_childs: bool = True,
    ):

        super().__init__(
            name=name,
            prototing=prototing,
            indexes=indexes,
            child_id=child_id,
            sort_childs=sort_childs,
            meta=meta,
        )

        self._subscription_namespace = subscription_namespace

        if self.tingistry is None:
            raise FrklException(
                msg=f"Can't create SubscripTing '{self.full_name}'",
                reason="No tingistry provided.",
            )

        Ting.event_central.subscribe_to_prototing(
            self._ting_created_event, self.tingistry, prototing, TingsEvent.TING_CREATED
        )
        Ting.event_central.subscribe_to_prototing(
            self._ting_removed_event, self.tingistry, prototing, TingsEvent.TING_REMOVED
        )

    def _matches_namespace(self, ting):

        if self._subscription_namespace is None:
            return True

        return self._subscription_namespace in ting.namespace

    def _ting_added(self, ting: Ting) -> None:
        pass

    def _ting_removed(self, ting: Ting) -> None:
        pass

    def _ting_updated(self, ting: Ting, current_values: Mapping[str, Any]):
        pass

    def _ting_removed_event(
        self, event_name: str, source: Ting, event_details: Mapping[str, Any]
    ):

        subject: Ting = event_details["ting"]
        if not self._matches_namespace(subject):
            return

        if not subject.prototing == self.child_prototing.full_name:
            return

        subject.unsubscribe_from(
            self._ting_updated_event, TingsEvent.TING_VALUES_CHANGED
        )

        self.remove_child(subject)
        self._ting_removed(subject)

    def _ting_created_event(
        self, event_name: str, source: Ting, event_details: Mapping[str, Any]
    ):

        new_ting: Ting = event_details["ting"]

        if not new_ting.prototing == self.child_prototing.full_name:
            return
        if not self._matches_namespace(new_ting):
            return

        new_ting.subscribe_to(self._ting_updated_event, TingsEvent.TING_VALUES_CHANGED)

        # TODO: this is not ideal...
        self.add_child(new_ting)

        self._ting_added(new_ting)

    def _ting_updated_event(
        self, event_name: str, source: Ting, event_details: Mapping[str, Any]
    ):

        subject = event_details["current"]
        self.update_child(source, invalidate_child=False)

        self._ting_updated(source, subject)
